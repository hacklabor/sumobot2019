#Konzepte



###1. SaugBot


Unter dem Roboter befinden sich zwei Sauggreifer.


>  Diese sind so gelagert, das sich der Roboter um diese drehen kann (Sauger = Mittelpunkt). Bewegungsbeispiel: Sauger 1 EIN Sauger 2 AUS--> drehen um Sauger 1 (zB90°) --> Sauger 2 EIN --> Sauger 1 Aus --> drehen um Sauger 2 und so weiter. Strategie: Mittelpunt der Spielfläche erreichen und sich ab und zu Bewegen um kein Timeout zu erhalten..... Der Rest ist "Aussitzen der Zeit" ODER Mitte erreichen und "Circle of Death" machen sprich sich schnell um eine Sauger drehen....;)

![Saugbot1](/Ideen/SaugBOT/konzept_Saugbot.PNG "")

Drehung könnte mit einem Rad mittig zwischen den Saugern oder mit zwei Rädern links und rechts neben den Saugern erfolgen (dann hätte man noch eine schnelle Fortbewgungsart)

Oder Intern indem die Sauger gedreht werden


![Saugbot2](/Ideen/SaugBOT/saugbot0Grad.png "")
![Saugbot3](/Ideen/SaugBOT/saugbot45Grad.png "")
![Saugbot4](/Ideen/SaugBOT/saugbot45Grad_Sauger2.png "")
 