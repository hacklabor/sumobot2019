// HC-05 Bluetooth Transponder
// Das Modul mit AT Kommandos programmieren
//
// Matthias Busse 17.6.2014 Version 1.0

/*
 * Bei Power ON--> Taster auf HC05 Board gedrückt Halten für AT Mode
 * 
AT+PSWD=""
AT+NAME:3.14
AT+UART=460800,0,0  


AT+ORGL                       : Auslieferungszustand
AT+UART?                      : UART Einstellung ausgeben
AT+UART=4800,0,0              : 4800 8 N 1
AT+UART=9600,0,0              : 9600 8 N 1
AT+UART=38400,0,0             : 38400 8 N 1
AT+PSWD?                      : Passwort anzeigen
AT+ROLE?                      : 0 ist Slave 1 ist Master
AT+NAME?                      : Name anzeigen
AT+NAME=HC-05                 : Namen geben (max 30 Zeichen)
AT+ADDR?                      : Adresse
AT+STATE?                     : Status?

AT+PSWD="H4ckl4b0r"
AT+NAME:3.14
AT+UART=460800,0,0  
 */

#include "SoftwareSerial.h"

SoftwareSerial mySerial(6, 7); // RX, TX
int i=0;

void setup() {
   pinMode(5, OUTPUT);
   digitalWrite(5,0 );
  pinMode(4, OUTPUT);
  digitalWrite(4, 1);
  
  mySerial.begin(38400);
  Serial.begin(38400);
  Serial.println("Komandos eingeben");
  delay(500);
  digitalWrite(9, 1); // Programmier-Pin für KEY aktiv
}

void loop() {
  if (mySerial.available()) // BT > PC
    Serial.write(mySerial.read());
  if (Serial.available())   // PC > BT
    mySerial.write(Serial.read());
}
