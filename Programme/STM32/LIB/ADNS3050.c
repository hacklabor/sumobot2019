/*
 * ADN3050.c
 *
 *  Created on: 10.05.2018
 *      Author: THK
 */

#include "ADNS3050.h"
#include "SPI_basic.h"

void ADNS3050_CS(GPIO_TypeDef* enable_GPIOx, uint16_t enable_GPIO_Pin, GPIO_PinState PinState){

	HAL_GPIO_WritePin(enable_GPIOx,enable_GPIO_Pin,PinState);

}


uint8_t ADNS3050_begin(SPI_HandleTypeDef *hspi,uint16_t timeout_ms){
  //--------Setup SPI Communication---------



 /*
  // set the details of the communication
  SPI.setBitOrder(MSBFIRST); // transimission order of bits
  SPI.setDataMode(SPI_MODE3); // sampling on rising edge
  SPI.setClockDivider(SPI_CLOCK_DIV16); // 16MHz/16 = 1MHz
  delay(10);
*/
  //----------------- Power Up and config ---------------
  SPI_status=0;
  uint8_t temp =SPI_readReg(hspi, PROD_ID  ,timeout_ms);
  HAL_Delay(100);
   temp =SPI_readReg(hspi, PROD_ID  ,timeout_ms);
  SPI_writeReg(hspi,RESET1|128,0x5a,timeout_ms);
   HAL_Delay(100); // wait for it to reboot
  SPI_writeReg(hspi,MOUSE_CTRL|128, 0x20,timeout_ms);//Setup Mouse Control
  SPI_writeReg(hspi,MOTION_CTRL|128, 0x00,timeout_ms);//Clear Motion Control register
  HAL_Delay(100);
  if (SPI_status==0){ return 1;}
  return 0;
  }





int8_t ADNS3050_getX(SPI_HandleTypeDef *hspi,uint16_t timeout_ms){//Prints out X and Y values to the serial terminal, use getX and getY sequentially for most operations

  int8_t temp =SPI_readReg(hspi,DELTA_X,timeout_ms);

       return temp;
}

int8_t ADNS3050_getY(SPI_HandleTypeDef *hspi,uint16_t timeout_ms){//Prints out X and Y values to the serial terminal, use getX and getY sequentially for most operations

  int8_t temp =SPI_readReg(hspi,DELTA_Y,timeout_ms);

       return temp;
}

uint8_t ADNS3050_Motion(SPI_HandleTypeDef *hspi,uint16_t timeout_ms){//Prints out X and Y values to the serial terminal, use getX and getY sequentially for most operations

  uint8_t temp =SPI_readReg(hspi,MOTION_ST,timeout_ms);

       return temp & 128;
}
uint8_t ADNS3050_SQUAL(SPI_HandleTypeDef *hspi,uint16_t timeout_ms){//Prints out X and Y values to the serial terminal, use getX and getY sequentially for most operations

  uint8_t temp =SPI_readReg(hspi,SQUAL,timeout_ms);

       return temp;
}
uint8_t ADNS3050_ID(SPI_HandleTypeDef *hspi,uint16_t timeout_ms){//Prints out X and Y values to the serial terminal, use getX and getY sequentially for most operations

  uint8_t temp =SPI_readReg(hspi, PROD_ID  ,timeout_ms);

       return temp;
}
