/*
 * i2c_basic.h
 *
 *  Created on: 05.05.2018
 *      Author: thoma
 */

#ifndef SPI_BASIC_H_
#define SPI_BASIC_H_

extern uint8_t SPI_status;

    void SPI_writeReg(SPI_HandleTypeDef *spi,uint8_t reg, uint8_t value,uint16_t timeout_ms);
    void SPI_writeReg16Bit(SPI_HandleTypeDef *spi,uint8_t reg, uint16_t value,uint16_t timeout_ms);
    void SPI_writeReg32Bit(SPI_HandleTypeDef *spi,uint8_t reg, uint32_t value,uint16_t timeout_ms);
    uint8_t SPI_readReg(SPI_HandleTypeDef *spi,uint8_t reg,uint16_t timeout_ms);
    uint16_t SPI_readReg16Bit(SPI_HandleTypeDef *spi,uint8_t reg,uint16_t timeout_ms);
    uint32_t SPI_readReg32Bit(SPI_HandleTypeDef *spi,uint8_t reg,uint16_t timeout_ms);

    void SPI_writeMulti(SPI_HandleTypeDef *spi,uint8_t reg, uint8_t const * src, uint8_t count,uint16_t timeout_ms);
    void SPI_readMulti(SPI_HandleTypeDef *spi,uint8_t reg, uint8_t * dst, uint8_t count,uint16_t timeout_ms);


#endif /* SPI_BASIC_H_ */
