/*
 * cc1101.c
 *
 *  Created on: 13.04.2018
 *      Author: thoma
 */

#include <CC1101.h>
#include "main.h"
#include "stdint.h"


/****************************************************************/
#define 	WRITE_BURST     	0x40						//write burst
#define 	READ_SINGLE     	0x80						//read single
#define 	READ_BURST      	0xC0						//read burst
#define 	BYTES_IN_RXFIFO     0x7F  						//byte number in RXfifo

/****************************************************************/
uint8_t PaTabel[8] = {0x60 ,0x60 ,0x60 ,0x60 ,0x60 ,0x60 ,0x60 ,0x60};





/****************************************************************
*FUNCTION NAME:Reset
*FUNCTION     :CC1101 reset //details refer datasheet of CC1101/CC1100//
*INPUT        :none
*OUTPUT       :none
****************************************************************/
uint8_t Reset (SPI_HandleTypeDef *hspi)
{
        uint8_t  ret;

		HAL_GPIO_WritePin(CS_PORT,CS_PIN,GPIO_PIN_RESET);
		HAL_Delay(1);
		HAL_GPIO_WritePin(CS_PORT,CS_PIN,GPIO_PIN_SET);
		HAL_Delay(1);
		ret = ChipSelect();

				if (ret!=ErrNoFault){
					return ret;
				}
			uint8_t message[0];
			message[0]=CC1101_SRES;
	HAL_SPI_Transmit( hspi, (uint8_t *)message,1, HAL_MAX_DELAY);


		HAL_GPIO_WritePin(CS_PORT,CS_PIN,GPIO_PIN_SET);
return ErrNoFault;
}

/****************************************************************
*FUNCTION NAME:Init
*FUNCTION     :CC1101 initialization
*INPUT        :none
*OUTPUT       :none
****************************************************************/
uint8_t CC1101_Init(SPI_HandleTypeDef *hspi)
{
    uint8_t ret;

	HAL_GPIO_WritePin(CS_PORT,CS_PIN,GPIO_PIN_SET);
	HAL_GPIO_WritePin(SCK_PORT,SCK_PIN,GPIO_PIN_SET);
	HAL_GPIO_WritePin(MOSI_PORT,MOSI_PIN,GPIO_PIN_RESET);
	ret=Reset(hspi);										//CC1101 reset
	ret=RegConfigSettings(hspi,F_433);							//CC1101 register config
	ret=SpiWriteBurstReg(hspi,CC1101_PATABLE,PaTabel,8);		//CC1101 PATABLE config

	return 1;
}


/****************************************************************
*FUNCTION NAME:SpiWriteReg
*FUNCTION     :CC1101 write data to register
*INPUT        :addr: register address; value: register value
*OUTPUT       :none
****************************************************************/
uint8_t SpiWriteReg(SPI_HandleTypeDef *hspi, uint8_t addr, uint8_t value)
{
	uint8_t ret;
	ret = ChipSelect() ;

		if (ret!=ErrNoFault){
			return ret;
		}

		uint8_t message[1];
		message[0]=addr;
		message[1]=value;
		HAL_SPI_Transmit( hspi, (uint8_t *)message,2, HAL_MAX_DELAY);


		HAL_GPIO_WritePin(CS_PORT,CS_PIN,GPIO_PIN_SET);
}

/****************************************************************
*FUNCTION NAME:SpiWriteBurstReg
*FUNCTION     :CC1101 write burst data to register
*INPUT        :addr: register address; buffer:register value array; num:number to write
*OUTPUT       :none
****************************************************************/
uint8_t SpiWriteBurstReg(SPI_HandleTypeDef *hspi,uint8_t addr, uint8_t *buffer, uint8_t num)
{
	uint8_t i,ret;


	ret = ChipSelect() ;

		if (ret!=1){
			return ret;
		};


		uint8_t message[0];

		message[0]=addr | WRITE_BURST;

	HAL_SPI_Transmit( hspi, (uint8_t *)message,1, HAL_MAX_DELAY);
	HAL_SPI_Transmit( hspi, (uint8_t *)buffer,num, HAL_MAX_DELAY);
    HAL_GPIO_WritePin(CS_PORT,CS_PIN,GPIO_PIN_SET);
}

/****************************************************************
*FUNCTION NAME:SpiStrobe
*FUNCTION     :CC1101 Strobe
*INPUT        :strobe: command; //refer define in CC1101.h//
*OUTPUT       :none
****************************************************************/
uint8_t SpiStrobe(SPI_HandleTypeDef *hspi,uint8_t strobe)
{
	uint8_t ret,tmp;
	ret = ChipSelect();

			if (ret!=ErrNoFault){
				return ret;
			}
	uint8_t message[0];
	uint8_t message1[0];
	message[0]=strobe;
	HAL_SPI_TransmitReceive( hspi, (uint8_t *)message,message1,1, HAL_MAX_DELAY);
	HAL_GPIO_WritePin(CS_PORT,CS_PIN,GPIO_PIN_SET);
	tmp=message1[0];
}

/****************************************************************
*FUNCTION NAME:SpiReadReg
*FUNCTION     :CC1101 read data from register
*INPUT        :addr: register address
*OUTPUT       :register value
****************************************************************/
uint8_t SpiReadReg(SPI_HandleTypeDef *hspi,uint8_t addr)
{
	uint8_t temp, value, ret;

    temp = addr|READ_SINGLE;
    ret = ChipSelect();

    		if (ret!=ErrNoFault){
    			return ret;
    		}
    		uint8_t message[0];
    		uint8_t rx[0];
    		message[0]= addr|READ_SINGLE;
    		HAL_SPI_Transmit( hspi, (uint8_t *)message,1, HAL_MAX_DELAY);
    		HAL_SPI_Receive( hspi, (uint8_t *)rx,1, HAL_MAX_DELAY);


	HAL_GPIO_WritePin(CS_PORT,CS_PIN,GPIO_PIN_SET);

	return rx[0];
}

/****************************************************************
*FUNCTION NAME:SpiReadBurstReg
*FUNCTION     :CC1101 read burst data from register
*INPUT        :addr: register address; buffer:array to store register value; num: number to read
*OUTPUT       :none
****************************************************************/
uint8_t SpiReadBurstReg(SPI_HandleTypeDef *hspi,uint8_t addr, uint8_t *buffer, uint8_t num)
{
	uint8_t i,temp,ret;

	temp = addr | READ_BURST;
	ret = ChipSelect;

			if (ret!=ErrNoFault){
				return ret;
			}

			uint8_t message[0];
			    		uint8_t rx[0];
			    		message[0]=addr | READ_BURST;
			    		HAL_SPI_Transmit( hspi, (uint8_t *)message,1, HAL_MAX_DELAY);
			    		HAL_SPI_Receive( hspi, (uint8_t *)buffer,num, HAL_MAX_DELAY);

	HAL_GPIO_WritePin(CS_PORT,CS_PIN,GPIO_PIN_SET);

}

/****************************************************************
*FUNCTION NAME:SpiReadStatus
*FUNCTION     :CC1101 read status register
*INPUT        :addr: register address
*OUTPUT       :status value
****************************************************************/
uint8_t SpiReadStatus(SPI_HandleTypeDef *hspi,uint8_t addr)
{
	uint8_t value,temp, ret;

	temp = addr | READ_BURST;

	ret = ChipSelect();

		if (ret!=ErrNoFault){
			return ret;
		}

	//SpiTransfer(temp);
//	value=SpiTransfer(0);
	HAL_GPIO_WritePin(CS_PORT,CS_PIN,GPIO_PIN_SET);

	return value;
}

/****************************************************************
*FUNCTION NAME:RegConfigSettings
*FUNCTION     :CC1101 register config //details refer datasheet of CC1101/CC1100//
*INPUT        :none
*OUTPUT       :none
****************************************************************/
uint8_t RegConfigSettings(SPI_HandleTypeDef *hspi, uint8_t f)
{
	    uint8_t ret;
	 ret=   SpiWriteReg(hspi,CC1101_FSCTRL1,  0x08);
	  ret=  SpiWriteReg(hspi,CC1101_FSCTRL0,  0x00);

	    switch(f)
	    {
	      case F_868:
	      	SpiWriteReg(hspi,CC1101_FREQ2,    F2_868);
	      	SpiWriteReg(hspi,CC1101_FREQ1,    F1_868);
	      	SpiWriteReg(hspi,CC1101_FREQ0,    F0_868);
	        break;
	      case F_915:
	        SpiWriteReg(hspi,CC1101_FREQ2,    F2_915);
	        SpiWriteReg(hspi,CC1101_FREQ1,    F1_915);
	        SpiWriteReg(hspi,CC1101_FREQ0,    F0_915);
	        break;
		  case F_433:
	        SpiWriteReg(hspi,CC1101_FREQ2,    F2_433);
	        SpiWriteReg(hspi,CC1101_FREQ1,    F1_433);
	        SpiWriteReg(hspi,CC1101_FREQ0,    F0_433);
	        break;
		  default: // F must be set
		  	break;
		}

	    SpiWriteReg(hspi,CC1101_MDMCFG4,  0x5B);
	    SpiWriteReg(hspi,CC1101_MDMCFG3,  0xF8);
	    SpiWriteReg(hspi,CC1101_MDMCFG2,  0x03);
	    SpiWriteReg(hspi,CC1101_MDMCFG1,  0x22);
	    SpiWriteReg(hspi,CC1101_MDMCFG0,  0xF8);
	    SpiWriteReg(hspi,CC1101_CHANNR,   0x00);
	    SpiWriteReg(hspi,CC1101_DEVIATN,  0x47);
	    SpiWriteReg(hspi,CC1101_FREND1,   0xB6);
	    SpiWriteReg(hspi,CC1101_FREND0,   0x10);
	    SpiWriteReg(hspi,CC1101_MCSM0 ,   0x18);
	    SpiWriteReg(hspi,CC1101_FOCCFG,   0x1D);
	    SpiWriteReg(hspi,CC1101_BSCFG,    0x1C);
	    SpiWriteReg(hspi,CC1101_AGCCTRL2, 0xC7);
		SpiWriteReg(hspi,CC1101_AGCCTRL1, 0x00);
	    SpiWriteReg(hspi,CC1101_AGCCTRL0, 0xB2);
	    SpiWriteReg(hspi,CC1101_FSCAL3,   0xEA);
		SpiWriteReg(hspi,CC1101_FSCAL2,   0x2A);
		SpiWriteReg(hspi,CC1101_FSCAL1,   0x00);
	    SpiWriteReg(hspi,CC1101_FSCAL0,   0x11);
	    SpiWriteReg(hspi,CC1101_FSTEST,   0x59);
	    SpiWriteReg(hspi,CC1101_TEST2,    0x81);
	    SpiWriteReg(hspi,CC1101_TEST1,    0x35);
	    SpiWriteReg(hspi,CC1101_TEST0,    0x09);
	    SpiWriteReg(hspi,CC1101_IOCFG2,   0x0B); 	//serial clock.synchronous to the data in synchronous serial mode
	    SpiWriteReg(hspi,CC1101_IOCFG0,   0x06);  	//asserts when sync word has been sent/received, and de-asserts at the end of the packet
	    SpiWriteReg(hspi,CC1101_PKTCTRL1, 0x04);		//two status bytes will be appended to the payload of the packet,including RSSI LQI and CRC OK
												//No address check
	    SpiWriteReg(hspi,CC1101_PKTCTRL0, 0x05);		//whitening off;CRC Enable£»variable length packets, packet length configured by the first byte after sync word
	    SpiWriteReg(hspi,CC1101_ADDR,     0x00);		//address used for packet filtration.
	    SpiWriteReg(hspi,CC1101_PKTLEN,   0x3D); 	//61 bytes max length

	    return ErrNoFault;
	}

/****************************************************************
*FUNCTION NAME:SendData
*FUNCTION     :use CC1101 send data
*INPUT        :txBuffer: data array to send; size: number of data to send, no more than 61
*OUTPUT       :none
****************************************************************/
uint8_t SendData(SPI_HandleTypeDef *hspi,uint8_t *txBuffer,uint8_t size)
{
	SpiWriteReg(hspi,CC1101_TXFIFO,size);
	SpiWriteBurstReg(hspi,CC1101_TXFIFO,txBuffer,size);			//write data to send
	SpiStrobe(hspi,CC1101_STX);									//start send
	CheckSENDFlag()	;				// Wait for GDO0 to be cleared -> end of packet
	SpiStrobe(hspi,CC1101_SFTX);									//flush TXfifo
}

/****************************************************************
*FUNCTION NAME:SetReceive
*FUNCTION     :set CC1101 to receive state
*INPUT        :none
*OUTPUT       :none
****************************************************************/
uint8_t SetReceive(SPI_HandleTypeDef *hspi)
{
	SpiStrobe(hspi, CC1101_SRX);
}

/****************************************************************
*FUNCTION NAME:CheckReceiveFlag
*FUNCTION     :check receive data or not
*INPUT        :none
*OUTPUT       :flag: 0 no data; 1 receive data
****************************************************************/
uint8_t CheckReceiveFlag(void)
{



	if(HAL_GPIO_ReadPin(GDO0_PORT,GDO0_PIN)	)	//receive data
	{
		while (HAL_GPIO_ReadPin(GDO0_PORT,GDO0_PIN));
		return 1;
	}
	else							// no data
	{
		return 0;
	}
}

uint8_t CheckSENDFlag(void)
{

while(1){
	if(HAL_GPIO_ReadPin(GDO0_PORT,GDO0_PIN)==1	)	//receive data
		{
		if(HAL_GPIO_ReadPin(GDO0_PORT,GDO0_PIN)==0	){
			return 1;
		}

		}

}


}
/****************************************************************
*FUNCTION NAME:ReceiveData
*FUNCTION     :read data received from RXfifo
*INPUT        :rxBuffer: buffer to store data
*OUTPUT       :size of data received
****************************************************************/
uint8_t ReceiveData(SPI_HandleTypeDef *hspi,uint8_t *rxBuffer)
{
	uint8_t size;
	uint8_t status[2];

	if(SpiReadStatus(hspi,CC1101_RXBYTES) & BYTES_IN_RXFIFO)
	{
		size=SpiReadReg(hspi,CC1101_RXFIFO);
		SpiReadBurstReg(hspi,CC1101_RXFIFO,rxBuffer,size);
		SpiReadBurstReg(hspi,CC1101_RXFIFO,status,2);
		SpiStrobe(hspi,CC1101_SFRX);
		return size;
	}
	else
	{
		SpiStrobe(hspi,CC1101_SFRX);
		return 0;
	}
return 1;
}

uint8_t ChipSelect (void)
{
uint8_t Localtimeout = 0;

HAL_GPIO_WritePin(CS_PORT,CS_PIN,GPIO_PIN_RESET);


while( HAL_GPIO_ReadPin(MISO_PORT,MISO_PIN)){
	HAL_Delay(1);
	Localtimeout ++;
	if (Localtimeout >= timeout){
	return ErrCsTimeout;
	}

}
return ErrNoFault;
}



