#ifndef ADNS3050_h
#define ADNS3050_h
#include "main.h"
#include "stm32f4xx.h"

 // SPI and misc pins for the ADNS



// Define all the registers

enum ADNS3050regAddr
    {
  PROD_ID                         = 0x00,
  REV_ID                          = 0x01,
  MOTION_ST                       = 0x02,
  DELTA_X                         = 0x03,
  DELTA_Y                         = 0x04,
  SQUAL                           = 0x05,
  SHUT_HI                         = 0x06,
  SHUT_LO                         = 0x07,
  PIX_MAX                         = 0x08,
  PIX_ACCUM                       = 0x09,
  PIX_MIN                         = 0x0a,
  PIX_GRAB                        = 0x0b,
  MOUSE_CTRL                      = 0x0d,
  RUN_DOWNSHIFT                   = 0x0e,
  REST1_PERIOD                    = 0x0f,
  REST1_DOWNSHIFT                 = 0x10,
  REST2_PERIOD                    = 0x11,
  REST2_DOWNSHIFT                 = 0x12,
  REST3_PERIOD                    = 0x13,
  PREFLASH_RUN                    = 0x14,
  PREFLASH_RUN_DARK               = 0x18,
  MOTION_EXT                      = 0x1b,
  SHUT_THR                        = 0x1c,
  SQUAL_THRESHOLD                 = 0x1d,
  NAV_CTRL2                       = 0x22,
  MISC_SETTINGS                   = 0x25,
  RESOLUTION                      = 0x33,
  LED_PRECHARGE                   = 0x34,
  FRAME_IDLE                      = 0x35,
  RESET1                          = 0x3a,
  INV_REV_ID                      = 0x3f,
  LED_CTRL                        = 0x40,
  MOTION_CTRL                     = 0x41,
  AUTO_LED_CONTROL                = 0x43,
  REST_MODE_CONFIG                = 0x45

    };



void ADNS3050_CS(GPIO_TypeDef* enable_GPIOx, uint16_t enable_GPIO_Pin, GPIO_PinState PinState);
uint8_t ADNS3050_begin(SPI_HandleTypeDef *hspi,uint16_t timeout_ms);
int8_t ADNS3050_getX(SPI_HandleTypeDef *hspi,uint16_t timeout_ms);
int8_t ADNS3050_getY(SPI_HandleTypeDef *hspi,uint16_t timeout_ms);
uint8_t ADNS3050_Motion(SPI_HandleTypeDef *hspi,uint16_t timeout_ms);
uint8_t ADNS3050_SQUAL(SPI_HandleTypeDef *hspi,uint16_t timeout_ms);
uint8_t ADNS3050_ID(SPI_HandleTypeDef *hspi,uint16_t timeout_ms);
#endif



