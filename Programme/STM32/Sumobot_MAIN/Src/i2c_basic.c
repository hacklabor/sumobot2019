/*
 * i2c_basic.c
 *
 *  Created on: 05.05.2018
 *      Author: THK
 *
 */

#include "stm32f4xx_hal.h"

uint8_t I2C_status;

// Write an 8-bit register
void writeReg(I2C_HandleTypeDef *hi2c,uint8_t address,uint8_t reg, uint8_t value,uint16_t timeout_ms)
{ uint8_t tx[2] ;


     tx[0]=reg;
     tx[1]=value;
	 I2C_status += HAL_I2C_Master_Transmit(hi2c,address <<1,tx,2,timeout_ms);


}

// Write a 16-bit register
void writeReg16Bit(I2C_HandleTypeDef *hi2c,uint8_t address, uint8_t reg, uint16_t value,uint16_t timeout_ms)
{

	uint8_t tx[3] ;
	tx[0]=reg;
	tx[1]=	(value >> 8) & 0xFF;
	tx[2]=  value       & 0xFF;

    I2C_status += HAL_I2C_Master_Transmit(hi2c,address<<1,tx,2,timeout_ms);

}

// Write a 32-bit register
void writeReg32Bit(I2C_HandleTypeDef *hi2c,uint8_t address, uint8_t reg, uint32_t value,uint16_t timeout_ms)
{


  uint8_t tx[5];
  	  tx[0]=reg;
	  tx[1]=	(value >> 24) & 0xFF;
  	  tx[2]=	(value >> 16) & 0xFF;
  	  tx[3]=	(value >>  8) & 0xFF;
  	  tx[4]=     value        & 0xFF;
  	  I2C_status += HAL_I2C_Master_Transmit(hi2c,address <<1,reg,1,timeout_ms);
      I2C_status += HAL_I2C_Master_Transmit(hi2c,address <<1,tx,4,timeout_ms);
}

// Read an 8-bit register
uint8_t readReg(I2C_HandleTypeDef *hi2c,uint8_t address,uint8_t reg,uint16_t timeout_ms)
{
  uint8_t value[1];
  uint8_t tx[1] ;
  tx[0]=reg;

  I2C_status += HAL_I2C_Master_Transmit(hi2c,address <<1,tx,1,timeout_ms);
  I2C_status += HAL_I2C_Master_Receive(hi2c,address <<1,value,1,timeout_ms);


  return value[0];
}

// Read a 16-bit register
uint16_t readReg16Bit(I2C_HandleTypeDef *hi2c,uint8_t address,uint8_t reg,uint16_t timeout_ms)
{
  uint16_t value;
  uint8_t rx[2] ;
  uint8_t tx[1] ;
  tx[0]=reg;

    I2C_status += HAL_I2C_Master_Transmit(hi2c,address <<1,tx,1,timeout_ms);
   I2C_status += HAL_I2C_Master_Receive(hi2c,address <<1,rx,2,timeout_ms);

  value  = (uint16_t)rx[0] << 8; // value high byte
  value |=          rx[1];      // value low byte

  return value;
}

// Read a 32-bit register
uint32_t readReg32Bit(I2C_HandleTypeDef *hi2c,uint8_t address,uint8_t reg,uint16_t timeout_ms)
{
  uint32_t value;
  uint8_t rx[4] ;
  uint8_t tx[1] ;
  tx[0]=reg;

    I2C_status += HAL_I2C_Master_Transmit(hi2c,address <<1,tx,1,timeout_ms);
  I2C_status += HAL_I2C_Master_Receive(hi2c,address <<1,rx,4,timeout_ms);


  value  = (uint32_t)rx[0] << 24; // value highest byte
  value |= (uint32_t)rx[1] << 16;
  value |= (uint16_t)rx[2] <<  8;
  value |=           rx[3];       // value lowest byte

  return value;
}

// Write an arbitrary number of bytes from the given array to the sensor,
// starting at the given register
void writeMulti(I2C_HandleTypeDef *hi2c,uint8_t address, uint8_t reg, uint8_t const * src, uint8_t count, uint16_t timeout_ms)
{
	 uint8_t tx[1] ;
	  tx[0]=reg;
	  I2C_status += HAL_I2C_Master_Transmit(hi2c,address <<1,tx,1,timeout_ms);
	  I2C_status += HAL_I2C_Master_Transmit(hi2c,address <<1,(uint8_t *)src,count,timeout_ms);

}

// Read an arbitrary number of bytes from the sensor, starting at the given
// register, into the given array
void readMulti(I2C_HandleTypeDef *hi2c,uint8_t address, uint8_t reg, uint8_t *dst, uint8_t count,uint16_t timeout_ms)
{
	 uint8_t tx[1] ;
	 tx[0]=reg;
	 I2C_status += HAL_I2C_Master_Transmit(hi2c,address <<1,tx,1,timeout_ms);
	 I2C_status += HAL_I2C_Master_Receive(hi2c,address <<1,(uint8_t *) dst,count,timeout_ms);
}
