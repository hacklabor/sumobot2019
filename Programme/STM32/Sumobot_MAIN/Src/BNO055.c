/***************************************************************************
  This is a library for the BNO055 orientation sensor

  Designed specifically to work with the Adafruit BNO055 Breakout.

  Pick one up today in the adafruit shop!
  ------> http://www.adafruit.com/products

  These sensors use I2C to communicate, 2 pins are required to interface.

  Adafruit invests time and resources providing this open source code,
  please support Adafruit andopen-source hardware by purchasing products
  from Adafruit!

  Written by KTOWN for Adafruit Industries.

  MIT license, all text above must be included in any redistribution
 ***************************************************************************/
#include <BNO055.h>
#include "i2c_basic.h"
#include "stm32f4xx_hal.h"


/**************************************************************************/
/*!
    @brief  Sets up the HW
*/
/**************************************************************************/
uint8_t BNO055_begin(I2C_HandleTypeDef *hi2c,uint8_t DeviceAddresse,uint8_t timeout_ms)
{
	I2C_status;
	bno055_opmode_t  mode =OPERATION_MODE_NDOF;
  /* Make sure we have the right device */
  uint8_t id = readReg(hi2c,DeviceAddresse,BNO055_CHIP_ID_ADDR,timeout_ms);
  if(id != BNO055_ID)
  {
   HAL_Delay(1000); // hold on for boot
    id = readReg(hi2c,DeviceAddresse,BNO055_CHIP_ID_ADDR,timeout_ms);
    if(id != BNO055_ID) {
      return 0;  // still not? ok bail
    }
  }

  /* Switch to config mode (just in case since this is the default) */
  BNO055_setMode(hi2c,DeviceAddresse,OPERATION_MODE_CONFIG,timeout_ms);

  /* Reset */
  writeReg(hi2c,DeviceAddresse,BNO055_SYS_TRIGGER_ADDR, 0x20,timeout_ms);
  while (readReg(hi2c,DeviceAddresse,BNO055_CHIP_ID_ADDR,timeout_ms) != BNO055_ID)
  {
    HAL_Delay(10);
  }
  HAL_Delay(50);

  /* Set to normal power mode */
  writeReg(hi2c,DeviceAddresse,BNO055_PWR_MODE_ADDR, POWER_MODE_NORMAL,timeout_ms);
  HAL_Delay(10);

  writeReg(hi2c,DeviceAddresse,BNO055_PAGE_ID_ADDR, 0,timeout_ms);

  /* Set the output units */
  /*
  uint8_t unitsel = (0 << 7) | // Orientation = Android
                    (0 << 4) | // Temperature = Celsius
                    (0 << 2) | // Euler = Degrees
                    (1 << 1) | // Gyro = Rads
                    (0 << 0);  // Accelerometer = m/s^2
  writeReg(hi2c,DeviceAddresse,BNO055_UNIT_SEL_ADDR, unitsel);
  */

  /* Configure axis mapping (see section 3.4) */
  /*
  writeReg(hi2c,DeviceAddresse,BNO055_AXIS_MAP_CONFIG_ADDR, REMAP_CONFIG_P2); // P0-P7, Default is P1
  HAL_Delay(10);
  writeReg(hi2c,DeviceAddresse,BNO055_AXIS_MAP_SIGN_ADDR, REMAP_SIGN_P2); // P0-P7, Default is P1
  HAL_Delay(10);
  */
  
  writeReg(hi2c,DeviceAddresse,BNO055_SYS_TRIGGER_ADDR, 0x0, timeout_ms);
  HAL_Delay(10);
  /* Set the requested operating mode (see section 3.3) */
  BNO055_setMode(hi2c,DeviceAddresse,mode,timeout_ms);
  HAL_Delay(20);
  if (I2C_status==0){ return 1;}
    return 0;
}

/**************************************************************************/
/*!
    @brief  Puts the chip in the specified operating mode
*/
/**************************************************************************/
void BNO055_setMode(I2C_HandleTypeDef *hi2c,uint8_t DeviceAddresse,bno055_opmode_t mode,uint8_t timeout_ms)
{

  writeReg(hi2c,DeviceAddresse,BNO055_OPR_MODE_ADDR, mode,timeout_ms);
  HAL_Delay(30);
}

/**************************************************************************/
/*!
    @brief  Use the external 32.768KHz crystal
*/
/**************************************************************************/
void BNO055_setExtCrystalUse(I2C_HandleTypeDef *hi2c,uint8_t DeviceAddresse,uint8_t usextal,uint8_t timeout_ms)


{
  bno055_opmode_t modeback = _mode;

  /* Switch to config mode (just in case since this is the default) */
  BNO055_setMode(hi2c,DeviceAddresse,OPERATION_MODE_CONFIG,timeout_ms);
  HAL_Delay(25);
  writeReg(hi2c,DeviceAddresse,BNO055_PAGE_ID_ADDR, 0,timeout_ms);
  if (usextal) {
    writeReg(hi2c,DeviceAddresse,BNO055_SYS_TRIGGER_ADDR, 0x80,timeout_ms);
  } else {
    writeReg(hi2c,DeviceAddresse,BNO055_SYS_TRIGGER_ADDR, 0x00,timeout_ms);
  }
  HAL_Delay(10);
  /* Set the requested operating mode (see section 3.3) */
  BNO055_setMode(hi2c,DeviceAddresse,modeback,timeout_ms);
  HAL_Delay(20);
}


/**************************************************************************/
/*!
    @brief  Gets the latest system status info
*/
/**************************************************************************/
void BNO055_getSystemStatus(I2C_HandleTypeDef *hi2c,uint8_t DeviceAddresse,uint8_t *system_status, uint8_t *self_test_result, uint8_t *system_error,uint8_t timeout_ms)
{
  writeReg(hi2c,DeviceAddresse,BNO055_PAGE_ID_ADDR, 0,timeout_ms);

  /* System Status (see section 4.3.58)
     ---------------------------------
     0 = Idle
     1 = System Error
     2 = Initializing Peripherals
     3 = System Iniitalization
     4 = Executing Self-Test
     5 = Sensor fusio algorithm running
     6 = System running without fusion algorithms */

  if (system_status != 0)
    *system_status    = readReg(hi2c,DeviceAddresse,BNO055_SYS_STAT_ADDR,timeout_ms);

  /* Self Test Results (see section )
     --------------------------------
     1 = test passed, 0 = test failed

     Bit 0 = Accelerometer self test
     Bit 1 = Magnetometer self test
     Bit 2 = Gyroscope self test
     Bit 3 = MCU self test

     0x0F = all good! */

  if (self_test_result != 0)
    *self_test_result = readReg(hi2c,DeviceAddresse,BNO055_SELFTEST_RESULT_ADDR,timeout_ms);

  /* System Error (see section 4.3.59)
     ---------------------------------
     0 = No error
     1 = Peripheral initialization error
     2 = System initialization error
     3 = Self test result failed
     4 = Register map value out of range
     5 = Register map address out of range
     6 = Register map write error
     7 = BNO low power mode not available for selected operat ion mode
     8 = Accelerometer power mode not available
     9 = Fusion algorithm configuration error
     A = Sensor configuration error */

  if (system_error != 0)
    *system_error     = readReg(hi2c,DeviceAddresse,BNO055_SYS_ERR_ADDR,timeout_ms);

  HAL_Delay(200);
}

/**************************************************************************/
/*!
    @brief  Gets the chip revision numbers
*/
/**************************************************************************/
void BNO055_getRevInfo(I2C_HandleTypeDef *hi2c,uint8_t DeviceAddresse,bno055_rev_info_t* info,uint8_t timeout_ms)
{
  uint8_t a, b;

  memset(info, 0, sizeof(bno055_rev_info_t));

  /* Check the accelerometer revision */
  info->accel_rev = readReg(hi2c,DeviceAddresse,BNO055_ACCEL_REV_ID_ADDR,timeout_ms);

  /* Check the magnetometer revision */
  info->mag_rev   = readReg(hi2c,DeviceAddresse,BNO055_MAG_REV_ID_ADDR,timeout_ms);

  /* Check the gyroscope revision */
  info->gyro_rev  = readReg(hi2c,DeviceAddresse,BNO055_GYRO_REV_ID_ADDR,timeout_ms);

  /* Check the SW revision */
  info->bl_rev    = readReg(hi2c,DeviceAddresse,BNO055_BL_REV_ID_ADDR,timeout_ms);

  a = readReg(hi2c,DeviceAddresse,BNO055_SW_REV_ID_LSB_ADDR,timeout_ms);
  b = readReg(hi2c,DeviceAddresse,BNO055_SW_REV_ID_MSB_ADDR,timeout_ms);
  info->sw_rev = (((uint16_t)b) << 8) | ((uint16_t)a);
}

/**************************************************************************/
/*!
    @brief  Gets current calibration state.  Each value should be a uint8_t
            pointer and it will be set to 0 if not calibrated and 3 if
            fully calibrated.
*/
/**************************************************************************/
uint8_t BNO055_getCalibration(I2C_HandleTypeDef *hi2c,uint8_t DeviceAddresse,uint8_t* sys, uint8_t* gyro, uint8_t* accel, uint8_t* mag,uint8_t timeout_ms)
{
  uint8_t calData = readReg(hi2c,DeviceAddresse,BNO055_CALIB_STAT_ADDR,timeout_ms);
  if (sys != NULL) {
    *sys = (calData >> 6) & 0x03;
  }
  if (gyro != NULL) {
    *gyro = (calData >> 4) & 0x03;
  }
  if (accel != NULL) {
    *accel = (calData >> 2) & 0x03;
  }
  if (mag != NULL) {
    *mag = calData & 0x03;
  }
  return (calData >> 6) & 0x03;
}

/**************************************************************************/
/*!
    @brief  Gets the temperature in degrees celsius
*/
/**************************************************************************/
int8_t BNO055_getTemp(I2C_HandleTypeDef *hi2c,uint8_t DeviceAddresse,uint8_t timeout_ms)
{
  int8_t temp = (int8_t)(readReg(hi2c,DeviceAddresse,BNO055_TEMP_ADDR,timeout_ms));
  return temp;
}

/**************************************************************************/
/*!
    @brief  Gets a vector reading from the specified source
*/
/**************************************************************************/
void BNO055_getVector(I2C_HandleTypeDef *hi2c,uint8_t DeviceAddresse,vector_type_t vector_type, double *xyz,uint8_t timeout_ms)
{

  uint8_t buffer[6];
  memset (buffer, 0, 6);

  int16_t x, y, z;
  x = y = z = 0;

  /* Read vector data (6 bytes) */
  readMulti(hi2c,DeviceAddresse,(bno055_reg_t)vector_type, buffer, 6,timeout_ms);

  x = ((int16_t)buffer[0]) | (((int16_t)buffer[1]) << 8);
  y = ((int16_t)buffer[2]) | (((int16_t)buffer[3]) << 8);
  z = ((int16_t)buffer[4]) | (((int16_t)buffer[5]) << 8);

  /* Convert the value to an appropriate range (section 3.6.4) */
  /* and assign the value to the Vector type */
  switch(vector_type)
  {
    case VECTOR_MAGNETOMETER:
      /* 1uT = 16 LSB */
      xyz[0] = ((double)x)/16.0;
      xyz[1] = ((double)y)/16.0;
      xyz[2] = ((double)z)/16.0;
      break;
    case VECTOR_GYROSCOPE:
      /* 1rps = 900 LSB */
      xyz[0] = ((double)x)/900.0;
      xyz[1] = ((double)y)/900.0;
      xyz[2] = ((double)z)/900.0;
      break;
    case VECTOR_EULER:
      /* 1 degree = 16 LSB */
      xyz[0] = ((double)x)/16.0;
      xyz[1] = ((double)y)/16.0;
      xyz[2] = ((double)z)/16.0;
      break;
    case VECTOR_ACCELEROMETER:
    case VECTOR_LINEARACCEL:
    case VECTOR_GRAVITY:
      /* 1m/s^2 = 100 LSB */
      xyz[0] = ((double)x)/100.0;
      xyz[1] = ((double)y)/100.0;
      xyz[2] = ((double)z)/100.0;
      break;
  }


}


/**************************************************************************/
/*!
    @brief  Gets a quaternion reading from the specified source
*/
/**************************************************************************/

void BNO055_getQuat(I2C_HandleTypeDef *hi2c,uint8_t DeviceAddresse,double *xyzw,uint8_t timeout_ms)
{
  uint8_t buffer[8];
  memset (buffer, 0, 8);

  int16_t x, y, z, w;
  x = y = z = w = 0;

  /* Read quat data (8 bytes) */
  readMulti(hi2c,DeviceAddresse,BNO055_QUATERNION_DATA_W_LSB_ADDR,buffer,6,timeout_ms);
  w = (((uint16_t)buffer[1]) << 8) | ((uint16_t)buffer[0]);
  x = (((uint16_t)buffer[3]) << 8) | ((uint16_t)buffer[2]);
  y = (((uint16_t)buffer[5]) << 8) | ((uint16_t)buffer[4]);
  z = (((uint16_t)buffer[7]) << 8) | ((uint16_t)buffer[6]);

  /* Assign to Quaternion */
  /* See http://ae-bst.resource.bosch.com/media/products/dokumente/bno055/BST_BNO055_DS000_12~1.pdf
     3.6.5.5 Orientation (Quaternion)  */
  const double scale = 1;//(1.0 / (1<<14));
  xyzw[0]= (double)x*scale;
  xyzw[1]= (double)y*scale;
  xyzw[2]= (double)z*scale;
  xyzw[3]= (double)w*scale;



}

/**************************************************************************/
/*!
    @brief  Provides the sensor_t data for this sensor
*/
/**************************************************************************/
void BNO055_getSensor(I2C_HandleTypeDef *hi2c,uint8_t DeviceAddresse,sensor_t *sensor,uint8_t timeout_ms)
{
  /* Clear the sensor_t object */
  memset(sensor, 0, sizeof(sensor_t));

  /* Insert the sensor name in the fixed length char array */
  strncpy (sensor->name, "BNO055", sizeof(sensor->name) - 1);
  sensor->name[sizeof(sensor->name)- 1] = 0;
  sensor->version     = 1;
  sensor->sensor_id   = _sensorID;
  sensor->type        = SENSOR_TYPE_ORIENTATION;
  sensor->min_delay   = 0;
  sensor->max_value   = 0.0F;
  sensor->min_value   = 0.0F;
  sensor->resolution  = 0.01F;
}

/**************************************************************************/
/*!
    @brief  Reads the sensor and returns the data as a sensors_event_t
*/
/**************************************************************************/
void BNO055_getEvent(I2C_HandleTypeDef *hi2c,uint8_t DeviceAddresse,vector_type_t vector_type,sensors_event_t *event,uint8_t timeout_ms)
{
  /* Clear the event */
  memset(event, 0, sizeof(sensors_event_t));
 double xyz[3];
  event->version   = sizeof(sensors_event_t);
  event->sensor_id = _sensorID;
  event->type      = SENSOR_TYPE_ORIENTATION;
  event->timestamp = 100;

  /* Get a Euler angle sample for orientation */

  BNO055_getVector(hi2c,DeviceAddresse,vector_type,xyz,timeout_ms);
  event->orientation.x = xyz[0];
  event->orientation.y = xyz[1];
  event->orientation.z = xyz[2];

  return 1;
}

/**************************************************************************/
/*!
@brief  Reads the sensor's offset registers into a byte array
*/
/**************************************************************************/
uint8_t BNO055_getSensorOffsets(I2C_HandleTypeDef *hi2c,uint8_t DeviceAddresse,uint8_t * calibData,uint8_t timeout_ms)
{
    if (isFullyCalibrated(hi2c,DeviceAddresse,timeout_ms))
    {
        bno055_opmode_t lastMode = _mode;

        BNO055_setMode(hi2c,DeviceAddresse,OPERATION_MODE_CONFIG,timeout_ms);
        readMulti(hi2c,DeviceAddresse,ACCEL_OFFSET_X_LSB_ADDR, calibData,22,timeout_ms);

        BNO055_setMode(hi2c,DeviceAddresse,lastMode,timeout_ms);
        return 1;
    }
    return 0;
}

/**************************************************************************/
/*!
@brief  Reads the sensor's offset registers into an offset struct
*/
/**************************************************************************/
/*
uint8_t BNO055_getSensorOffsets1(I2C_HandleTypeDef *hi2c,uint8_t DeviceAddresse,bno055_offsets_t &offsets_type,uint8_t timeout_ms)
{
    if (isFullyCalibrated())
    {
        bno055_opmode_t lastMode = _mode;
        BNO055_setMode(OPERATION_MODE_CONFIG);
        HAL_Delay(25);

        offsets_type.accel_offset_x = (readReg(hi2c,DeviceAddresse,ACCEL_OFFSET_X_MSB_ADDR) << 8) | (readReg(hi2c,DeviceAddresse,ACCEL_OFFSET_X_LSB_ADDR));
        offsets_type.accel_offset_y = (readReg(hi2c,DeviceAddresse,ACCEL_OFFSET_Y_MSB_ADDR) << 8) | (readReg(hi2c,DeviceAddresse,ACCEL_OFFSET_Y_LSB_ADDR));
        offsets_type.accel_offset_z = (readReg(hi2c,DeviceAddresse,ACCEL_OFFSET_Z_MSB_ADDR) << 8) | (readReg(hi2c,DeviceAddresse,ACCEL_OFFSET_Z_LSB_ADDR));

        offsets_type.gyro_offset_x = (readReg(hi2c,DeviceAddresse,GYRO_OFFSET_X_MSB_ADDR) << 8) | (readReg(hi2c,DeviceAddresse,GYRO_OFFSET_X_LSB_ADDR));
        offsets_type.gyro_offset_y = (readReg(hi2c,DeviceAddresse,GYRO_OFFSET_Y_MSB_ADDR) << 8) | (readReg(hi2c,DeviceAddresse,GYRO_OFFSET_Y_LSB_ADDR));
        offsets_type.gyro_offset_z = (readReg(hi2c,DeviceAddresse,GYRO_OFFSET_Z_MSB_ADDR) << 8) | (readReg(hi2c,DeviceAddresse,GYRO_OFFSET_Z_LSB_ADDR));

        offsets_type.mag_offset_x = (readReg(hi2c,DeviceAddresse,MAG_OFFSET_X_MSB_ADDR) << 8) | (readReg(hi2c,DeviceAddresse,MAG_OFFSET_X_LSB_ADDR));
        offsets_type.mag_offset_y = (readReg(hi2c,DeviceAddresse,MAG_OFFSET_Y_MSB_ADDR) << 8) | (readReg(hi2c,DeviceAddresse,MAG_OFFSET_Y_LSB_ADDR));
        offsets_type.mag_offset_z = (readReg(hi2c,DeviceAddresse,MAG_OFFSET_Z_MSB_ADDR) << 8) | (readReg(hi2c,DeviceAddresse,MAG_OFFSET_Z_LSB_ADDR));

        offsets_type.accel_radius = (readReg(hi2c,DeviceAddresse,ACCEL_RADIUS_MSB_ADDR) << 8) | (readReg(hi2c,DeviceAddresse,ACCEL_RADIUS_LSB_ADDR));
        offsets_type.mag_radius = (readReg(hi2c,DeviceAddresse,MAG_RADIUS_MSB_ADDR) << 8) | (readReg(hi2c,DeviceAddresse,MAG_RADIUS_LSB_ADDR));

        BNO055_setMode(hi2c,DeviceAddresse,lastMode,timeout_ms);
        return 1;
    }
    return 0;
}
*/


/**************************************************************************/
/*!
@brief  Writes an array of calibration values to the sensor's offset registers
*/
/**************************************************************************/
void BNO055_setSensorOffsets(I2C_HandleTypeDef *hi2c,uint8_t DeviceAddresse,uint8_t* calibData,uint8_t timeout_ms)
{
    bno055_opmode_t lastMode = _mode;
    BNO055_setMode(hi2c,DeviceAddresse,OPERATION_MODE_CONFIG,timeout_ms);

    HAL_Delay(25);

    /* A writeLen() would make this much cleaner */
    writeMulti(hi2c,DeviceAddresse,ACCEL_OFFSET_X_LSB_ADDR,calibData,22,timeout_ms);
/*
    writeReg(hi2c,DeviceAddresse,ACCEL_OFFSET_X_LSB_ADDR, calibData[0],t timeout_ms);
    writeReg(hi2c,DeviceAddresse,ACCEL_OFFSET_X_MSB_ADDR, calibData[1],t timeout_ms);
    writeReg(hi2c,DeviceAddresse,ACCEL_OFFSET_Y_LSB_ADDR, calibData[2],t timeout_ms);
    writeReg(hi2c,DeviceAddresse,ACCEL_OFFSET_Y_MSB_ADDR, calibData[3],t timeout_ms);
    writeReg(hi2c,DeviceAddresse,ACCEL_OFFSET_Z_LSB_ADDR, calibData[4],t timeout_ms);
    writeReg(hi2c,DeviceAddresse,ACCEL_OFFSET_Z_MSB_ADDR, calibData[5],t timeout_ms);

    writeReg(hi2c,DeviceAddresse,GYRO_OFFSET_X_LSB_ADDR, calibData[6],t timeout_ms);
    writeReg(hi2c,DeviceAddresse,GYRO_OFFSET_X_MSB_ADDR, calibData[7],t timeout_ms);
    writeReg(hi2c,DeviceAddresse,GYRO_OFFSET_Y_LSB_ADDR, calibData[8],t timeout_ms);
    writeReg(hi2c,DeviceAddresse,GYRO_OFFSET_Y_MSB_ADDR, calibData[9],t timeout_ms);
    writeReg(hi2c,DeviceAddresse,GYRO_OFFSET_Z_LSB_ADDR, calibData[10],t timeout_ms);
    writeReg(hi2c,DeviceAddresse,GYRO_OFFSET_Z_MSB_ADDR, calibData[11],t timeout_ms);

    writeReg(hi2c,DeviceAddresse,MAG_OFFSET_X_LSB_ADDR, calibData[12],t timeout_ms);
    writeReg(hi2c,DeviceAddresse,MAG_OFFSET_X_MSB_ADDR, calibData[13],t timeout_ms);
    writeReg(hi2c,DeviceAddresse,MAG_OFFSET_Y_LSB_ADDR, calibData[14],t timeout_ms);
    writeReg(hi2c,DeviceAddresse,MAG_OFFSET_Y_MSB_ADDR, calibData[15],t timeout_ms);
    writeReg(hi2c,DeviceAddresse,MAG_OFFSET_Z_LSB_ADDR, calibData[16],t timeout_ms);
    writeReg(hi2c,DeviceAddresse,MAG_OFFSET_Z_MSB_ADDR, calibData[17],t timeout_ms);

    writeReg(hi2c,DeviceAddresse,ACCEL_RADIUS_LSB_ADDR, calibData[18],t timeout_ms);
    writeReg(hi2c,DeviceAddresse,ACCEL_RADIUS_MSB_ADDR, calibData[19],t timeout_ms);

    writeReg(hi2c,DeviceAddresse,MAG_RADIUS_LSB_ADDR, calibData[20],t timeout_ms);
    writeReg(hi2c,DeviceAddresse,MAG_RADIUS_MSB_ADDR, calibData[21]),t timeout_ms);
*/
    BNO055_setMode(hi2c,DeviceAddresse,lastMode,timeout_ms);
}

uint8_t  BNO055_isFullyCalibrated(I2C_HandleTypeDef *hi2c,uint8_t DeviceAddresse, uint8_t timeout_ms)
{
    uint8_t system, gyro, accel, mag;
    BNO055_getCalibration(hi2c,DeviceAddresse,&system, &gyro, &accel, &mag,timeout_ms);
    if (system < 3 || gyro < 3 || accel < 3 || mag < 3)
        return 0;
    return 1;
}


/**************************************************************************/
/*!
@brief  Writes to the sensor's offset registers from an offset struct
*/
/**************************************************************************/
/*
void BNO055_setSensorOffsets1(I2C_HandleTypeDef *hi2c,uint8_t DeviceAddresse,bno055_offsets_t &offsets_type,uint8_t timeout_ms)
{
    bno055_opmode_t lastMode = _mode;
    BNO055_setMode(OPERATION_MODE_CONFIG);
    HAL_Delay(25);

    writeReg(hi2c,DeviceAddresse,ACCEL_OFFSET_X_LSB_ADDR, (offsets_type.accel_offset_x) & 0x0FF);
    writeReg(hi2c,DeviceAddresse,ACCEL_OFFSET_X_MSB_ADDR, (offsets_type.accel_offset_x >> 8) & 0x0FF);
    writeReg(hi2c,DeviceAddresse,ACCEL_OFFSET_Y_LSB_ADDR, (offsets_type.accel_offset_y) & 0x0FF);
    writeReg(hi2c,DeviceAddresse,ACCEL_OFFSET_Y_MSB_ADDR, (offsets_type.accel_offset_y >> 8) & 0x0FF);
    writeReg(hi2c,DeviceAddresse,ACCEL_OFFSET_Z_LSB_ADDR, (offsets_type.accel_offset_z) & 0x0FF);
    writeReg(hi2c,DeviceAddresse,ACCEL_OFFSET_Z_MSB_ADDR, (offsets_type.accel_offset_z >> 8) & 0x0FF);

    writeReg(hi2c,DeviceAddresse,GYRO_OFFSET_X_LSB_ADDR, (offsets_type.gyro_offset_x) & 0x0FF);
    writeReg(hi2c,DeviceAddresse,GYRO_OFFSET_X_MSB_ADDR, (offsets_type.gyro_offset_x >> 8) & 0x0FF);
    writeReg(hi2c,DeviceAddresse,GYRO_OFFSET_Y_LSB_ADDR, (offsets_type.gyro_offset_y) & 0x0FF);
    writeReg(hi2c,DeviceAddresse,GYRO_OFFSET_Y_MSB_ADDR, (offsets_type.gyro_offset_y >> 8) & 0x0FF);
    writeReg(hi2c,DeviceAddresse,GYRO_OFFSET_Z_LSB_ADDR, (offsets_type.gyro_offset_z) & 0x0FF);
    writeReg(hi2c,DeviceAddresse,GYRO_OFFSET_Z_MSB_ADDR, (offsets_type.gyro_offset_z >> 8) & 0x0FF);

    writeReg(hi2c,DeviceAddresse,MAG_OFFSET_X_LSB_ADDR, (offsets_type.mag_offset_x) & 0x0FF);
    writeReg(hi2c,DeviceAddresse,MAG_OFFSET_X_MSB_ADDR, (offsets_type.mag_offset_x >> 8) & 0x0FF);
    writeReg(hi2c,DeviceAddresse,MAG_OFFSET_Y_LSB_ADDR, (offsets_type.mag_offset_y) & 0x0FF);
    writeReg(hi2c,DeviceAddresse,MAG_OFFSET_Y_MSB_ADDR, (offsets_type.mag_offset_y >> 8) & 0x0FF);
    writeReg(hi2c,DeviceAddresse,MAG_OFFSET_Z_LSB_ADDR, (offsets_type.mag_offset_z) & 0x0FF);
    writeReg(hi2c,DeviceAddresse,MAG_OFFSET_Z_MSB_ADDR, (offsets_type.mag_offset_z >> 8) & 0x0FF);

    writeReg(hi2c,DeviceAddresse,ACCEL_RADIUS_LSB_ADDR, (offsets_type.accel_radius) & 0x0FF);
    writeReg(hi2c,DeviceAddresse,ACCEL_RADIUS_MSB_ADDR, (offsets_type.accel_radius >> 8) & 0x0FF);

    writeReg(hi2c,DeviceAddresse,MAG_RADIUS_LSB_ADDR, (offsets_type.mag_radius) & 0x0FF);
    writeReg(hi2c,DeviceAddresse,MAG_RADIUS_MSB_ADDR, (offsets_type.mag_radius >> 8) & 0x0FF);

    BNO055_setMode(hi2c,DeviceAddresse,lastMode,timeout_ms);
}
*/




