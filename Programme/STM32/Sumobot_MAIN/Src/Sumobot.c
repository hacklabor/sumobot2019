/*
 * Sumobot.c
 *
 *  Created on: 05.05.2018
 *      Author: THK
 *
 */
#include "Sumobot.h"
#include "main.h"
#include "stm32f4xx_hal.h"

UART_HandleTypeDef huart3;

int Timer1_max 		= 100;  // Intrupt  5ys * 100  = 5ms = 2kHz
int Timer1_akt 		= 0;
int Timer2_max 		= 4000; // Interupt 5ys * 4000 = 20ms = 50Hz
int Timer2_akt 		= 0;
int Servo1			= 500;
int Servo2 			= 150;
int Motor1_PWM		= 50;
int Motor2_PWM		= 50;
int VAC_PUMPE_PWM	= 100;
int Saug1_hoch 		= 150;
int Saug1_runter 	= 225;
int Saug2_hoch 		= 500;
int Saug2_runter 	= 400;
int VL53L0_aktuell_mm[8];
int VL53L0_letzte_mm[8]= {0,0,0,0,0,0,0,0};
double BNO055_xyzw[4] = {0,0,0,0};
uint8_t system1, gyro1, accel1, mag1;
volatile uint8_t InitDone =0;
struct botStruct bot;


uint16_t Sensor_exist = 0;     // Bit0 = VL53L0x[0].... Bit7 = VL53L0x[7]; Bit8 = BNO055; Bit9 =Mouse1; Bit10=Mouse2; Bit11= Analog Digital

uint8_t VL53L0X_low_distance, VL53L0X_med_distance,VL53L0X_high_distance, VL53L0X_fast_diff;

sensors_event_t BNO055_VectorEuler, BNO055_VectorMAG,BNO055_VectorGYRO,BNO055_VectorGrav;
 int Mouse1XY[2],Mouse2XY[2];
// Buffer f�r Bluetooth �bertragung

uint8_t DMA_RX_Buffer[DMA_RX_BUFFER_SIZE];
char TelemetryHeader[]=";#H#;Servo1;Servo2;Sensor_exist;BNO55_Rx;BNO55_Ry;BNO055_Rz;Mouse1Y,Mouse1X;Mouse2Y,Mouse2X;VL53L0X_LowDistance,VL53L0X_MedDistance;VL53L0X_HighDistance,VL53L0X_fast_diff;VL53L0X N;VL53L0X NO;VL53L0X O;VL53L0X SO;VL53L0X S;VL53L0X SW;VL53L0X W;VL53L0X NW;#E#";

uint8_t UART_Buffer[UART_BUFFER_SIZE];



uint32_t VL53L0X_GPIO[8] =  {LIDAR_N_GPIO_Port,LIDAR_NO_GPIO_Port ,LIDAR_O_GPIO_Port,LIDAR_SO_GPIO_Port,LIDAR_S_GPIO_Port,LIDAR_SW_GPIO_Port,LIDAR_W_GPIO_Port,LIDAR_NW_GPIO_Port};
uint16_t VL32L0X_PIN[8]  =  {LIDAR_N_Pin,LIDAR_NO_Pin ,LIDAR_O_Pin,LIDAR_SO_Pin,LIDAR_S_Pin,LIDAR_SW_Pin,LIDAR_W_Pin,LIDAR_NW_Pin};

/*************************************************************************************************************************************************************/
/**
  * @brief BOT
  * @retval None
  */
/*************************************************************************************************************************************************************/

void BotInit (void){

        bot.x=50;


	 	HAL_UART_Transmit(&huart3,(uint8_t *)TelemetryHeader,strlen(TelemetryHeader),10);

	 	//display

	    SSD1306_Init(&hi2c1,&hdma_i2c1_tx,100);
	    SSD1306_GotoXY(0,0);

	    SSD1306_Puts("Sumobot 2019", &Font_7x10, 1);


	    SSD1306_UpdateScreen(&hi2c1,&hdma_i2c1_tx);

	    // Vakum Aus

	    HAL_GPIO_WritePin(VAC1_ON_GPIO_Port,VAC1_ON_Pin,GPIO_PIN_RESET);
	    HAL_GPIO_WritePin(VAC2_ON_GPIO_Port,VAC2_ON_Pin,GPIO_PIN_RESET);

	    // Sauger anheben

	    Servo1 = 430;
	    Servo2 = 170;
	    HAL_Delay(1000);

	    // Sauger Servo aus

	    Servo1 = 430;
	    Servo2 =45000;
	 //   HAL_GPIO_WritePin(GPIOB,GPIO_PIN_13,GPIO_PIN_SET);
	    HAL_Delay(10);

	    BNO055_INIT();
	     // BNO055_Check_CALIBRATION ();

	    // Entfernugssensoren
	    VL53L0_SetAdress();
	    VL53L0_INIT();
	    InitDone=1;
}



/*************************************************************************************************************************************************************/
/**
  * @brief BNO055 9Achsen Gyroskope, Beschleunigungssensor I2C
  * @retval None
  */
/*************************************************************************************************************************************************************/

void BNO055_INIT (void)
{
	Sensor_exist |= BNO055_begin(&hi2c1,BNO055_I2C_Address,10)<<8;


#if debug==1
	 sensor_t sensor;
	  BNO055_getSensor(&hi2c1,BNO055_I2C_Address,&sensor,10);
	  printf  ("------------------------------------\n");
	  printf  ("Sensor		: %s\n",sensor.name);
	  printf  ("Driver Ver	: %lu\n",sensor.version);
	  printf  ("Unique ID	: %lu\n",sensor.sensor_id);
	  printf  ("Max Value	: %4.8f\n",sensor.max_value);
	  printf  ("Min Value	: %4.8f\n",sensor.min_value);
	  printf  ("Resolution  : %4.8f\n",sensor.resolution);
	  printf("------------------------------------\n");
	  printf("\n");
#endif
}

void BNO055_EVENT (void)
{

	  BNO055_getEvent(&hi2c1,BNO055_I2C_Address,VECTOR_EULER,&BNO055_VectorEuler,10);
	  BNO055_getEvent(&hi2c1,BNO055_I2C_Address,VECTOR_MAGNETOMETER,&BNO055_VectorMAG,10);
	  BNO055_getEvent(&hi2c1,BNO055_I2C_Address,VECTOR_GRAVITY,&BNO055_VectorGrav,10);
	  BNO055_getEvent(&hi2c1,BNO055_I2C_Address,VECTOR_GYROSCOPE,&BNO055_VectorGYRO,10);
	  char txt[20] ={'\0'};
	  		  sprintf(txt,"x   : %3.1f,  %3.1f               ",BNO055_VectorEuler.orientation.x,BNO055_VectorMAG.orientation.x);
	          SSD1306_Text(0,30,txt, &Font_7x10, 1);

	          sprintf(txt,"y   : %3.1f,  %3.1f               ",BNO055_VectorEuler.orientation.y,BNO055_VectorMAG.orientation.y);
	          SSD1306_Text(0,40,txt, &Font_7x10, 1);
	          sprintf(txt,"z   : %3.1f,  %3.1f            ",BNO055_VectorEuler.orientation.z,BNO055_VectorMAG.orientation.z);
	          SSD1306_Text(0,50,txt, &Font_7x10, 1);
	          SSD1306_UpdateScreen(&hi2c1,&hdma_i2c1_tx);
#if debug==1

	  printf  ("Ex: %4.8f\n",BNO055_VectorEuler.orientation.x);
	  printf  ("Ey: %4.8f\n",BNO055_VectorEuler.orientation.y);
	  printf  ("Ez: %4.8f\n",BNO055_VectorEuler.orientation.z);
	  printf("------------------------------------\n");
	  printf("\n");
	  printf  ("Mx: %4.8f\n",BNO055_VectorMAG.orientation.x);
	  printf  ("My: %4.8f\n",BNO055_VectorMAG.orientation.y);
	  printf  ("Mz: %4.8f\n",BNO055_VectorMAG.orientation.z);
	  printf("------------------------------------\n");
	  printf("\n");
#endif
}

void BNO055_ABSOLUT (void)
{

  	  BNO055_getQuat(&hi2c1,BNO055_I2C_Address,BNO055_xyzw,10);
#if debug==1

  	  printf  ("X: %4.8f\n",BNO055_xyzw[0]);
  	  printf  ("Y: %4.8f\n",BNO055_xyzw[1]);
  	  printf  ("Z: %4.8f\n",BNO055_xyzw[2]);
  	  printf  ("W: %4.8f\n",BNO055_xyzw[3]);
  	  printf("------------------------------------\n");
  	  printf("\n");
#endif

}
void BNO055_Check_CALIBRATION (void)
{
	    SSD1306_GotoXY(0,15);
        SSD1306_Puts("BNO055 Klaibrierung", &Font_7x10, 1);
        SSD1306_UpdateScreen(&hi2c1,&hdma_i2c1_tx);

while   (!BNO055_isFullyCalibrated(&hi2c1,BNO055_I2C_Address,10))
		{
		BNO055_getCalibration(&hi2c1,BNO055_I2C_Address,&system1, &gyro1, &accel1, &mag1,10);
		char txt[20] ={'\0'};
		sprintf(txt,"Gyro   : %d",gyro1);
        SSD1306_Text(0,30,txt, &Font_7x10, 1);
        sprintf(txt,"accel  : %d",accel1);
        SSD1306_Text(0,40,txt, &Font_7x10, 1);
        sprintf(txt,"mag    : %d",mag1);
        SSD1306_Text(0,50,txt, &Font_7x10, 1);
        SSD1306_UpdateScreen(&hi2c1,&hdma_i2c1_tx);
#if debug==1

	printf  ("System: %d\n",system1);
	printf  ("Gyro	: %d\n",gyro1);
	printf  ("accel	: %d\n",accel1);
	printf  ("mag	: %d\n",mag1);
	printf("------------------------------------\n");
	printf("\n");
#endif
	HAL_Delay(100);
}

}



/*************************************************************************************************************************************************************/
/**
  * @brief VL53L0X Entfernungssensor I2C
  * @retval None
  */
/*************************************************************************************************************************************************************/
void VL53L0_SetAdress(void)
{
	 VL53L0_enable_PIN(GPIO_PIN_RESET);
	 for (int i=0;i<8;i++)
	 	{
	 		uint8_t Addr =VL53L0X_I2C_Start_Address +i;
	 		HAL_GPIO_WritePin(VL53L0X_GPIO[i],VL32L0X_PIN[i],GPIO_PIN_SET);
	 		HAL_Delay(10);
	 		setAddress(&hi2c1,Addr,100);
	 	}
}

void VL53L0_INIT (void){


  	    HAL_Delay(10);

	for (int i=0;i<8;i++)
	{
		uint8_t Addr =VL53L0X_I2C_Start_Address +i;
	    uint8_t res=(VL53L0xinit(&hi2c1,Addr,100))<<i;
	    Sensor_exist |=  res;
	    startContinuous(&hi2c1,Addr,0,100);
	}

}

void VL53L0_enable_PIN (GPIO_PinState PinState){
	for (int i=0; i<8;i++)
	{
		HAL_GPIO_WritePin(VL53L0X_GPIO[i],VL32L0X_PIN[i],PinState);
	}
}

void VL53L0_GetDistance (void)
{
	for (int i=0;i<8;i++)
	{
		if (Sensor_exist & 1<<i)
		{
		uint8_t Addr =VL53L0X_I2C_Start_Address +i;

		VL53L0_aktuell_mm[i]=	readRangeContinuousMillimeters(&hi2c1,Addr,Vl53l0X_Read_Timeout);
		int dif=(VL53L0_letzte_mm[i]-VL53L0_aktuell_mm[i])/(0.05);
		if (dif>VL52L0X_V_max)
		{
			VL53L0X_fast_diff |= 1<<i;
		}
		if (VL53L0_aktuell_mm[i]<VL52L0X_LOW)
		{
			VL53L0X_low_distance |= 1<<i;
		}
		else
		{
			VL53L0X_low_distance &= 255-(1<<i);
		}

		if (VL53L0_aktuell_mm[i]<VL52L0X_MED)
				{
					VL53L0X_med_distance |= 1<<i;
				}
				else
				{
					VL53L0X_med_distance &= 255-(1<<i);
				}

        #if debug==1
		printf("Sensor %d ",i);
		printf(" %d mm\n",VL53L0_aktuell_mm[i]);
        #endif
		VL53L0_letzte_mm[i]=VL53L0_aktuell_mm[i];
		}
	}
}
/*************************************************************************************************************************************************************/
/**
  * @brief Telematic �bertragen mit BlueTooth
  * @retval None
  */
/*************************************************************************************************************************************************************/
void Telematic(void){

    int pos = 0;
	const int sizeTxData =200;
	uint8_t TxData[200]=";#S#;";
	for (int i=5; i<sizeTxData; i++){
		TxData[i]='0';
	}
    pos +=5;
	Int2HEX (TxData+pos,Servo1);
	pos +=5;
	Int2HEX (TxData+pos,Servo2);
	pos +=5;
	Int2HEX (TxData+pos,Sensor_exist);
	pos +=5;
	Int2HEX (TxData+pos,BNO055_VectorEuler.orientation.x);
    pos +=5;
    Int2HEX (TxData+pos,BNO055_VectorEuler.orientation.y);
    pos +=5;
    Int2HEX (TxData+pos,BNO055_VectorEuler.orientation.z);
    pos +=5;
    Int2HEX (TxData+pos,Byte2uint16(Mouse1XY[1],Mouse1XY[0]));
    pos +=5;
    Int2HEX (TxData+pos,Byte2uint16(Mouse2XY[1],Mouse2XY[0]));
    pos +=5;
    Int2HEX (TxData+pos,Byte2uint16(VL53L0X_low_distance,VL53L0X_med_distance));
    pos +=5;

	Int2HEX (TxData+pos,Byte2uint16(VL53L0X_low_distance,VL53L0X_med_distance));
	pos +=5;
	Int2HEX (TxData+pos,Byte2uint16(VL53L0X_high_distance,VL53L0X_fast_diff));
	pos +=5;



	for (int i=0;i<8;i++)
	{
		Int2HEX (TxData+pos,VL53L0_aktuell_mm[i]);
		pos +=5;
	}

	TxData[sizeTxData-5]=';';
	TxData[sizeTxData-4]='#';
	TxData[sizeTxData-3]='E';
	TxData[sizeTxData-2]='#';
	TxData[sizeTxData-1]=10;


	HAL_UART_Transmit(&huart3,(uint8_t *)TxData,sizeTxData,10);
	//HAL_UART_Transmit_DMA(&huart3,(uint8_t *)TxData,sizeTxData);
}
void Int2HEX (uint8_t *dest,int Quelle){


	char hexval[5];
	sprintf(hexval,"%0x",Quelle);
	int len= strlen (hexval);
	len =4-len;
    for (int i=0; i<4;i++){
       if (i<len){
    	   dest[i]='0';
       }
       else
       {
    	   dest[i] = hexval[i-len];
       }
    }

    dest[4]=';';
  //  &pos += 5;
}

uint16_t Byte2uint16 (uint8_t byte1,uint8_t byte0)
{
	uint16_t value;

	value |= (uint16_t) byte1 <<  8;
	value |=            byte0      ;   			    // value lowest byte

	return value;
}
void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart){
	HAL_UART_DMAStop(&huart3);
}

void HAL_TIM_PeriodElapsedCallback( TIM_HandleTypeDef *htim){
	if (htim->Instance==TIM3){
	//TIM3 Task

		// SEGGER_SYSVIEW_RecordEnterISR(); //emit Enter ISR signal

		if(Timer1_akt<Motor1_PWM){
		 	HAL_GPIO_WritePin(M1_PWM_GPIO_Port,M1_PWM_Pin,GPIO_PIN_SET);
			}
		else {
			HAL_GPIO_WritePin(M1_PWM_GPIO_Port,M1_PWM_Pin,GPIO_PIN_RESET);
				 	}
		if(Timer1_akt<Motor2_PWM){
			HAL_GPIO_WritePin(M2_PWM_GPIO_Port,M2_PWM_Pin,GPIO_PIN_SET);
						 	}
		else {
			HAL_GPIO_WritePin(M2_PWM_GPIO_Port,M2_PWM_Pin,GPIO_PIN_RESET);
			}
		if(Timer1_akt<VAC_PUMPE_PWM){
			HAL_GPIO_WritePin(VAC_PWM_GPIO_Port,VAC_PWM_Pin,GPIO_PIN_RESET);
								 	}
		else {
			HAL_GPIO_WritePin(VAC_PWM_GPIO_Port,VAC_PWM_Pin,GPIO_PIN_SET);
			}

		Timer1_akt	+=	1;

		 if(Timer1_akt==Timer1_max){
			 Timer1_akt=0;
			}




		 if(Timer2_akt<Servo1){
		 	HAL_GPIO_WritePin(SERVO1_GPIO_Port,SERVO1_Pin,GPIO_PIN_SET);
		 	}
		 else {
		 	HAL_GPIO_WritePin(SERVO1_GPIO_Port,SERVO1_Pin, GPIO_PIN_RESET);
		 	}
		 if(Timer2_akt<Servo2){
		 	HAL_GPIO_WritePin(Servo2_GPIO_Port,Servo2_Pin,GPIO_PIN_SET);
		 	}
		 else {
		 	HAL_GPIO_WritePin(Servo2_GPIO_Port,Servo2_Pin,GPIO_PIN_RESET);
		 	 }


		 Timer2_akt	+=	1;
		 if(Timer2_akt==Timer2_max){
		 	Timer2_akt=0;
		 	}

		// SEGGER_SYSVIEW_RecordExitISR(); //emit Enter ISR signal



	}
}



int Motor (int Speed,GPIO_TypeDef* GPIOxDIR1, uint16_t GPIO_Pin_DIR1,GPIO_TypeDef* GPIOxDIR2, uint16_t GPIO_Pin_DIR2 )
{

	if (Speed<0){

	     HAL_GPIO_WritePin(GPIOxDIR1,GPIO_Pin_DIR1,GPIO_PIN_SET);
	     HAL_GPIO_WritePin(GPIOxDIR2,GPIO_Pin_DIR2,GPIO_PIN_RESET);
		 return (Speed *(-1));
	}
	else{
		HAL_GPIO_WritePin(GPIOxDIR1,GPIO_Pin_DIR1,GPIO_PIN_RESET);
		HAL_GPIO_WritePin(GPIOxDIR2,GPIO_Pin_DIR2,GPIO_PIN_SET);
		return Speed;
		  }
	return Speed;
}

