/*
 * Sumobot.h
 *
 *  Created on: 05.05.2018
 *      Author: thoma
 */

#ifndef SUMOBOT_H_
#define SUMOBOT_H_
#include "main.h"
#include "stm32f4xx_hal.h"
#include "VL53L0X.h"
#include "BNO055.h"
//#include "ADNS3050.h"
#include "ssd1306.h"


#define VL53L0X_I2C_Start_Address      0x2A
#define BNO055_I2C_Address      0x28
#define debug 1
#define Vl53l0X_Read_Timeout 1
#define main_delay 200
#define VL52L0X_V_max 100
#define VL52L0X_LOW   150
#define VL52L0X_MED   300


#define DMA_RX_BUFFER_SIZE          64
#define UART_BUFFER_SIZE            256

I2C_HandleTypeDef hi2c1;
DMA_HandleTypeDef hdma_i2c1_tx;
//SPI_HandleTypeDef hspi2;

extern char TelemetryHeader[];
extern uint8_t VL53L0_exist;     // Entfernungsensor Bit0 = Sensor 0 Bit7 = Sensor8
// Buffer für Bluetooth Übertragung
struct botStruct {
	uint16_t BotDurchmesser, RadAbstand, SaugerAbstand, SaugerDurchmesser, Radius_Linienerkennung;
	double   x, y, Rz_Absolut, Rz_Relativ_MP, X_Sauger1, Y_Sauger1, X_Sauger2, Y_Sauger2;
	uint8_t  VAC1_EIN, VAC2_EIN, VAC1_OK, VAC2_OK, VAC1_OK_Merker, VAC2_OK_Merker,Motor1_Speed, Motor2_Speed;
	 uint8_t  Lininenerkennung_OK,  Abstandssensoren_Green, Abstandssensoren_YellowAlert,Abstandssensoren_RedAlert;
}  ;
extern struct botStruct bot;

extern uint8_t DMA_RX_Buffer[DMA_RX_BUFFER_SIZE];


extern uint8_t UART_Buffer[UART_BUFFER_SIZE];



extern uint32_t VL53L0X_GPIO[8] ;
extern uint16_t VL32L0X_PIN[8] ;


extern int Timer1_max;
extern int Timer1_akt;
extern int Timer2_max;
extern int Timer2_akt;
extern int Servo1;
extern int Servo2;
extern int Motor1_PWM;
extern int Motor2_PWM;
extern int VAC_PUMPE_PWM;
extern int Saug1_hoch;
extern int Saug1_runter;
extern int Saug2_hoch;
extern int Saug2_runter;
extern int VL53L0_aktuell_mm[8];
extern double BNO055_xyzw[4];
extern uint8_t VL53L0X_low_distance, VL53L0X_med_distance,VL53L0X_high_distance, VL53L0X_fast_diff;
extern sensors_event_t BNO055_VectorEuler, BNO055_VectorMAG,BNO055_VectorGYRO,BNO055_VectorGrav;
extern uint8_t system1, gyro1, accel1, mag1;
extern int Mouse1XY[2],Mouse2XY[2];
extern volatile uint8_t InitDone;

// BOT

void BotInit (void);
int Motor (int Speed,GPIO_TypeDef* GPIOxDIR1, uint16_t GPIO_Pin_DIR1,GPIO_TypeDef* GPIOxDIR2, uint16_t GPIO_Pin_DIR2 );

// ADNS 3050 Mousesensor

void ADNS3050_INIT (void);


// BNO055 9 Achsen Gyro

void BNO055_INIT (void);
void BNO055_EVENT (void);
void BNO055_ABSOLUT (void);
void BNO055_Check_CALIBRATION (void);


// VL53L0 Entfernungssensoren

void VL53L0_SetAdress(void);
void VL53L0_INIT (void);
void VL53L0_enable_PIN (GPIO_PinState PinState);
void VL53L0_GetDistance (void);

// Blutooth Datenübertragung

void Telematic(void);
void Int2HEX (uint8_t *dest,int Quelle);
uint16_t Byte2uint16 (uint8_t byte1,uint8_t byte0);

// Interrupt

void HAL_TIM_PeriodElapsedCallback( TIM_HandleTypeDef *htim);
void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart);
void IRQ_MouseData (void);


#endif /* SUMOBOT_H_ */
