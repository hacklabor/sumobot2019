/*
 * i2c_basic.h
 *
 *  Created on: 05.05.2018
 *      Author: thoma
 */

#ifndef I2C_BASIC_H_
#define I2C_BASIC_H_

extern uint8_t I2C_status;

    void writeReg(I2C_HandleTypeDef *hi2c,uint8_t address,uint8_t reg, uint8_t value,uint16_t timeout_ms);
    void writeReg16Bit(I2C_HandleTypeDef *hi2c,uint8_t address,uint8_t reg, uint16_t value,uint16_t timeout_ms);
    void writeReg32Bit(I2C_HandleTypeDef *hi2c,uint8_t address,uint8_t reg, uint32_t value,uint16_t timeout_ms);
    uint8_t readReg(I2C_HandleTypeDef *hi2c,uint8_t address,uint8_t reg,uint16_t timeout_ms);
    uint16_t readReg16Bit(I2C_HandleTypeDef *hi2c,uint8_t address,uint8_t reg,uint16_t timeout_ms);
    uint32_t readReg32Bit(I2C_HandleTypeDef *hi2c,uint8_t address,uint8_t reg,uint16_t timeout_ms);

    void writeMulti(I2C_HandleTypeDef *hi2c,uint8_t address,uint8_t reg, uint8_t const * src, uint8_t count,uint16_t timeout_ms);
    void readMulti(I2C_HandleTypeDef *hi2c,uint8_t address,uint8_t reg, uint8_t * dst, uint8_t count,uint16_t timeout_ms);


#endif /* I2C_BASIC_H_ */
