Configuration	Sumobot_MAIN
STM32CubeMX 	4.26.0
Date	08/15/2018
MCU	STM32F446RETx



PERIPHERALS	MODES	FUNCTIONS	PINS
I2C1	I2C	I2C1_SCL	PB8
I2C1	I2C	I2C1_SDA	PB9
I2C2	I2C	I2C2_SCL	PB10
I2C2	I2C	I2C2_SDA	PC12
RCC	Crystal/Ceramic Resonator	RCC_OSC_IN	PH0-OSC_IN
RCC	Crystal/Ceramic Resonator	RCC_OSC_OUT	PH1-OSC_OUT
RCC	Crystal/Ceramic Resonator	RCC_OSC32_IN	PC14-OSC32_IN
RCC	Crystal/Ceramic Resonator	RCC_OSC32_OUT	PC15-OSC32_OUT
SPI2	Full-Duplex Master	SPI2_MISO	PB14
SPI2	Full-Duplex Master	SPI2_MOSI	PB15
SPI2	Full-Duplex Master	SPI2_SCK	PB13
SYS	Serial Wire	SYS_JTCK-SWCLK	PA14
SYS	Serial Wire	SYS_JTMS-SWDIO	PA13
SYS	SysTick	SYS_VS_Systick	VP_SYS_VS_Systick
TIM3	Internal Clock	TIM3_VS_ClockSourceINT	VP_TIM3_VS_ClockSourceINT
USART2	Asynchronous	USART2_RX	PA3
USART2	Asynchronous	USART2_TX	PA2
USART3	Asynchronous	USART3_RX	PC11
USART3	Asynchronous	USART3_TX	PC10



Pin Nb	PINs	FUNCTIONs	LABELs
2	PC13	GPIO_EXTI13	B1 [Blue PushButton]
3	PC14-OSC32_IN	RCC_OSC32_IN	
4	PC15-OSC32_OUT	RCC_OSC32_OUT	
5	PH0-OSC_IN	RCC_OSC_IN	
6	PH1-OSC_OUT	RCC_OSC_OUT	
8	PC0	GPIO_Output	VAC1_ON
9	PC1	GPIO_Output	VAC2_ON
10	PC2	GPIO_Input	VAC1_OK
11	PC3	GPIO_Input	VAC2_OK
14	PA0-WKUP	GPIO_EXTI0	VAC_RET
15	PA1	GPIO_Output	VAC_PWM
16	PA2	USART2_TX	USART_TX
17	PA3	USART2_RX	USART_RX
21	PA5	GPIO_Input	LO2
22	PA6	GPIO_Input	LO1
23	PA7	GPIO_Output	M2_PWM
24	PC4	GPIO_Output	Servo3
25	PC5	GPIO_Output	LIDAR_SO
27	PB1	GPIO_Output	Servo4
28	PB2	GPIO_Output	LIDAR_NW
29	PB10	I2C2_SCL	
33	PB12	GPIO_Output	LIDAR_W
34	PB13	SPI2_SCK	
35	PB14	SPI2_MISO	
36	PB15	SPI2_MOSI	
37	PC6	GPIO_Output	LIDAR_O
38	PC7	GPIO_Output	M2_DIR1
39	PC8	GPIO_Output	LIDAR_NO
40	PC9	GPIO_Output	LIDAR_N
41	PA8	GPIO_Output	M1_DIR2
42	PA9	GPIO_Output	M1_PWM
43	PA10	GPIO_Output	Servo2
44	PA11	GPIO_Output	LIDAR_SW
45	PA12	GPIO_Output	LIDAR_S
46	PA13	SYS_JTMS-SWDIO	TMS
49	PA14	SYS_JTCK-SWCLK	TCK
51	PC10	USART3_TX	
52	PC11	USART3_RX	
53	PC12	I2C2_SDA	
55	PB3*	SYS_JTDO-SWO	SWO
56	PB4	GPIO_Output	M1_DIR1
57	PB5	GPIO_Output	SERVO1
58	PB6	GPIO_Output	M2_DIR2
61	PB8	I2C1_SCL	
62	PB9	I2C1_SDA	



SOFTWARE PROJECT

Project Settings : 
Project Name : Sumobot_MAIN
Project Folder : D:\Projekte\sumobot2019\Programme\STM32\Sumobot_MAIN
Toolchain / IDE : TrueSTUDIO
Firmware Package Name and Version : STM32Cube FW_F4 V1.21.0


Code Generation Settings : 
STM32Cube Firmware Library Package : Copy only the necessary library files
Generate peripheral initialization as a pair of '.c/.h' files per peripheral : No
Backup previously generated files when re-generating : No
Delete previously generated files when not re-generated : Yes
Set all free pins as analog (to optimize the power consumption) : No


Toolchains Settings : 
Compiler Optimizations : Balanced Size/Speed






