/*
The MIT License (MIT)

Copyright (c) 2015 Maximilian Schmidt

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <unistd.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>

#define LEAF 0
#define AND 1
#define OR 2

#define FAILURE 0
#define SUCCESS 1
#define RUNNING 2

struct node
{   
	int type;
	int current_child_index;
	int child_count;
	struct node** children;
	int(*action)(void* agent);
}; 

struct agent
{
	int hunger;
};

int Tick(struct node *node, void* agent)
{
	if (node->type == LEAF)
		return node->action(agent);

	if (node->type == AND)
	{
		struct node* child = node->children[node->current_child_index];
		int state = Tick(child, agent);

		if (state == SUCCESS && node->current_child_index == (node->child_count - 1))
		{
			node->current_child_index = 0;
			return SUCCESS;
		}
		if (state == SUCCESS)
		{
			node->current_child_index++;
			return RUNNING;
		}
		if (state == RUNNING)
		{
			return RUNNING;
		}
		if (state == FAILURE)
		{
			node->current_child_index = 0;
			return FAILURE;
		}
	}

	if (node->type == OR)
	{
		struct node* child = node->children[node->current_child_index];
		int state = Tick(child, agent);

		if (state == SUCCESS)
		{
			node->current_child_index = 0;
			return SUCCESS;
		}
		if (state == FAILURE && node->current_child_index == (node->child_count - 1))
		{
			node->current_child_index = 0;
			return FAILURE;
		}
		if (state == FAILURE)
		{
			node->current_child_index++;
			return RUNNING;
		}
		if (state == RUNNING)
		{
			return RUNNING;
		}
	}

	return 0;
};


struct node* CreateLeaf(int(*action)(void* agent))
{
	struct node* node = calloc(1, sizeof(struct node));
	node->type = LEAF;
	node->action = action;
	return node;
}

struct node* CreateBranch(int branch_type, int child_count, struct node* child, ...)
{
	struct node* branch = calloc(1, sizeof(struct node));
	branch->type = branch_type;

	branch->children = malloc(sizeof(struct node*));
	branch->children[branch->child_count++] = child;

	va_list ap;
	va_start(ap, child);

	if (child_count > 1)
	while(branch->child_count < child_count)
	{
		struct node* c = va_arg(ap, struct node*);
		branch->children = realloc(branch->children, sizeof(struct node*) * (branch->child_count + 1));
		branch->children[branch->child_count++] = c;
	}

	va_end(ap);
	return branch;
}

int RandomState()
{
	int state = rand() % 3;
	if (state == FAILURE) 
		printf("\t failure\n");
	if (state == SUCCESS) 
		printf("\t success\n");
	if (state == RUNNING) 
		printf("\t running\n");

	return state;
}

int FindFoodNearby(void* agent)	{ printf("FindFood"); 		return RandomState(); }
int MoveToFood(void* agent)		{ printf("MoveToFood"); 	return RandomState(); }
int PickUpFood(void* agent)		{ printf("PickUpFood"); 	return RandomState(); }
int GoOut(void* agent)			{ printf("GoOut"); 			return RandomState(); }
int EatFood(void* agent)		{ printf("EatFood\t"); 		return RandomState(); }

int main()
{
	struct agent agent;
	agent.hunger = 1;

	struct node* leaf_findfoodnearby = CreateLeaf(FindFoodNearby);
	struct node* leaf_movetofood = CreateLeaf(MoveToFood);
	struct node* leaf_pickupfood = CreateLeaf(PickUpFood);
	struct node* leaf_goout = CreateLeaf(GoOut);
	struct node* leaf_eatfood = CreateLeaf(EatFood);

	struct node* branch_getnearbyfood = 
	CreateBranch(AND, 3, leaf_findfoodnearby, leaf_movetofood, leaf_pickupfood);

	struct node* branch_getfood =
	CreateBranch(OR, 2, branch_getnearbyfood, leaf_goout);

	struct node* root_stophunger = 
	CreateBranch(AND, 2, branch_getfood, leaf_eatfood);

	while(1)
	{
		Tick(root_stophunger, &agent);
		usleep(1000 * 1000 * 1);
	}
}
