﻿



Public Class BT_Visu

    Dim txt As String
    Dim lastNode As TreeNode


    Private Sub BtAdd_Click(sender As Object, e As EventArgs) Handles BtAdd.Click
        txt = CbTyp.Text + "-" + TbName.Text
        Try
            Tv1.SelectedNode.Nodes.Add(txt)
        Catch ex As Exception
            Tv1.Nodes.Add(txt)
        End Try
    End Sub

    Private Sub BtDel_Click(sender As Object, e As EventArgs) Handles BtDel.Click
        Tv1.SelectedNode.Remove()
    End Sub

    Private Sub BtLoad_Click(sender As Object, e As EventArgs) Handles BtLoad.Click
        If OFiD1.ShowDialog = DialogResult.OK Then
            Tv1.Nodes.Clear()
            treesaveload.LoadFromXml(OFiD1.FileName, Tv1)
        End If
    End Sub

    Private Sub Bt_Save_Click(sender As Object, e As EventArgs) Handles Bt_Save.Click
        If SFD1.ShowDialog = DialogResult.OK Then
            treesaveload.SaveToXml(SFD1.FileName, Tv1)
        End If
    End Sub

    Private Sub TbName_TextChanged(sender As Object, e As EventArgs) Handles TbName.TextChanged
        If TbName.Text.IndexOf("-") > -1 Then
            TbName.Text = TbName.Text.Replace("-", "")
            MsgBox("'-' nicht erlabtes Zeichen")
        End If
    End Sub



    Private Sub Tv1_AfterSelect(sender As Object, e As TreeViewEventArgs) Handles Tv1.AfterSelect
        If IsNothing(lastNode) = False Then
            lastNode.BackColor = Tv1.BackColor
        End If
        lastNode = Tv1.SelectedNode
        Dim txt1() As String
        Tv1.SelectedNode.BackColor = Color.Lime
        txt1 = Tv1.SelectedNode.Text.Split("-")
        CbTyp.Text = txt1(0)
        TbName.Text = txt1(1)
    End Sub

    Private Sub BtÄndern_Click(sender As Object, e As EventArgs) Handles BtÄndern.Click

        txt = CbTyp.Text + "-" + TbName.Text
        Tv1.SelectedNode.Text = txt
    End Sub
End Class