﻿Imports System.Xml
Public Class treesaveload



    ' XML-Datei auslesen und die entsprechenden 
    ' Nodes-Objekte im TreeView-Control erstellen
    Public Shared Sub LoadFromXml(ByVal FileName As String, ByVal TheTreeView As TreeView)
        Dim xDoc As New XmlDocument
        xDoc.Load(FileName)
        FillTreeView(TheTreeView.Nodes, xDoc.DocumentElement)
    End Sub

    Private Shared Sub FillTreeView(ByVal CurrentNodes As TreeNodeCollection,
          ByVal xNode As XmlNode)
        For Each xChild As XmlNode In xNode.ChildNodes
            FillTreeView(CurrentNodes.Add(xChild.Name).Nodes, xChild)
        Next
    End Sub

    ' TreeView-Inhalt (Nodes-Objekte) als XML-Datei speichern
    Public Shared Sub SaveToXml(ByVal FileName As String, ByVal TheTreeView As TreeView)
        Dim xDoc As New XmlDocument
        xDoc.LoadXml("<Nodes></Nodes>")
        SaveNodes(xDoc.DocumentElement, TheTreeView.Nodes)
        xDoc.Save(FileName)
    End Sub

    Private Shared Sub SaveNodes(ByVal xNode As XmlNode, ByVal CurrentNodes As TreeNodeCollection)
        For Each tn As TreeNode In CurrentNodes
            Debug.WriteLine(tn.Text)
            SaveNodes(xNode.AppendChild(xNode.OwnerDocument.CreateElement(tn.Text)), tn.Nodes)
        Next
    End Sub
End Class

