﻿Public Class Zeichnen
    Shared Sub drawKreis(ByRef g As Graphics, Durchmesser As Integer, X As Integer, Y As Integer, X_Offset As Integer, Y_Offset As Integer, Farbe As Color)
        Dim bru As New SolidBrush(Farbe)
        Dim x1, y1 As Single
        x1 = X_Offset + X - Durchmesser / 2
        y1 = Y_Offset - Y - Durchmesser / 2
        g.FillPie(bru, x1, y1, Durchmesser, Durchmesser, 0, 360)
    End Sub

    Shared Sub drawLinieBot(ByRef g As Graphics, bot As Main.BotStruct, X_Offset As Integer, Y_Offset As Integer, Farbe As Color)
        Dim bru As New SolidBrush(Farbe)
        Dim Pe As New Pen(bru)
        Dim x, y As Integer
        Dim drawFont As New Font("Arial", 32)

        x = rechnen.X_von_RadiusUndWinkel(bot.BotDurchmesser / 2, bot.Rz_Absolut + 90)
        y = rechnen.Y_von_RadiusUndWinkel(bot.BotDurchmesser / 2, bot.Rz_Absolut + 90)

        g.DrawLine(Pe, X_Offset + CSng(bot.x), Y_Offset - CSng(bot.y), X_Offset + CSng(bot.x - x), Y_Offset - CSng(bot.y + y))
        g.DrawString(bot.Rz_Absolut.ToString("000.0") + "°", drawFont, bru, X_Offset + CSng(bot.x - x), Y_Offset - CSng(bot.y + y))
        g.DrawString("X: " + bot.x.ToString("0000"), drawFont, bru, X_Offset + CSng(bot.x - 80), Y_Offset - CSng(bot.y))
        g.DrawString("Y: " + bot.y.ToString("0000"), drawFont, bru, X_Offset + CSng(bot.x - 80), Y_Offset - CSng(bot.y + 40))
        g.DrawLine(Pe, X_Offset + CSng(bot.x), Y_Offset - CSng(bot.y), X_Offset, Y_Offset)
        g.DrawString("     " + bot.Rz_Relativ_MP.ToString("000.0") + "°", drawFont, bru, X_Offset, Y_Offset)
    End Sub
    Shared Sub drawLinieLidar(ByRef g As Graphics, bot As Main.BotStruct, X_Offset As Integer, Y_Offset As Integer, p2 As Point, Farbe As Color)
        Dim bru As New SolidBrush(Farbe)
        Dim Pe As New Pen(bru)
        Pe.Width = 3
        Dim drawFont As New Font("Arial", 32)



        g.DrawLine(Pe, X_Offset + CSng(bot.x), Y_Offset - CSng(bot.y), X_Offset + p2.X, Y_Offset - p2.Y)

    End Sub
    Shared Sub drawBot(ByRef g As Graphics, Bot As Main.BotStruct, Lidar() As Main.LidarStruct, X_Offset As Integer, Y_Offset As Integer)

        'Bot Zeichnen
        drawKreis(g, Bot.BotDurchmesser, Bot.x, Bot.y, X_Offset, Y_Offset, Bot.BotFarbe)

        'Sauger1
        Dim Farbe As Color
        If Bot.VAC1_OK = True Then
            Farbe = Bot.SaugerFarbeEin
        Else
            Farbe = Bot.SaugerFarbeAus
        End If
        drawKreis(g, 40, Bot.X_Sauger1, Bot.Y_Sauger1, X_Offset, Y_Offset, Farbe)
        'Sauger2
        If Bot.VAC2_OK = True Then
            Farbe = Bot.SaugerFarbeEin
        Else
            Farbe = Bot.SaugerFarbeAus
        End If
        drawKreis(g, 40, Bot.X_Sauger2, Bot.Y_Sauger2, X_Offset, Y_Offset, Farbe)

        'Winkellinien
        drawLinieBot(g, Bot, X_Offset, Y_Offset, Bot.WinkelFarbe)
        'For i = 0 To 7
        '    drawLinieLidar(g, Bot, X_Offset, Y_Offset, Lidar(i).P, Lidar(i).Farbe)
        'Next
        drawLinieLidar(g, Bot, X_Offset, Y_Offset, Lidar(0).P(0), Lidar(0).Farbe)
        drawLinieLidar(g, Bot, X_Offset, Y_Offset, Lidar(0).P(1), Lidar(1).Farbe)

    End Sub
End Class
