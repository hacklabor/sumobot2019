﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BT_Visu
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.BtAdd = New System.Windows.Forms.Button()
        Me.BtDel = New System.Windows.Forms.Button()
        Me.CbTyp = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TbName = New System.Windows.Forms.TextBox()
        Me.Tv1 = New System.Windows.Forms.TreeView()
        Me.Bt_Save = New System.Windows.Forms.Button()
        Me.BtLoad = New System.Windows.Forms.Button()
        Me.DS_BT = New System.Data.DataSet()
        Me.DataTable1 = New System.Data.DataTable()
        Me.DataColumn1 = New System.Data.DataColumn()
        Me.DataColumn2 = New System.Data.DataColumn()
        Me.DataColumn3 = New System.Data.DataColumn()
        Me.DataColumn4 = New System.Data.DataColumn()
        Me.DataColumn5 = New System.Data.DataColumn()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.OFiD1 = New System.Windows.Forms.OpenFileDialog()
        Me.SFD1 = New System.Windows.Forms.SaveFileDialog()
        Me.BtÄndern = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        CType(Me.DS_BT, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataTable1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.BtÄndern)
        Me.GroupBox1.Controls.Add(Me.BtAdd)
        Me.GroupBox1.Controls.Add(Me.BtDel)
        Me.GroupBox1.Controls.Add(Me.CbTyp)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.TbName)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 507)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(415, 196)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "GroupBox1"
        '
        'BtAdd
        '
        Me.BtAdd.Location = New System.Drawing.Point(10, 76)
        Me.BtAdd.Name = "BtAdd"
        Me.BtAdd.Size = New System.Drawing.Size(75, 23)
        Me.BtAdd.TabIndex = 5
        Me.BtAdd.Text = "Add"
        Me.BtAdd.UseVisualStyleBackColor = True
        '
        'BtDel
        '
        Me.BtDel.Location = New System.Drawing.Point(6, 157)
        Me.BtDel.Name = "BtDel"
        Me.BtDel.Size = New System.Drawing.Size(75, 23)
        Me.BtDel.TabIndex = 4
        Me.BtDel.Text = "Delete"
        Me.BtDel.UseVisualStyleBackColor = True
        '
        'CbTyp
        '
        Me.CbTyp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CbTyp.FormattingEnabled = True
        Me.CbTyp.Items.AddRange(New Object() {"LEAF", "AND", "OR"})
        Me.CbTyp.Location = New System.Drawing.Point(119, 38)
        Me.CbTyp.Name = "CbTyp"
        Me.CbTyp.Size = New System.Drawing.Size(121, 21)
        Me.CbTyp.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(116, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(20, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Art"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(7, 20)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(35, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Name"
        '
        'TbName
        '
        Me.TbName.Location = New System.Drawing.Point(8, 38)
        Me.TbName.Name = "TbName"
        Me.TbName.Size = New System.Drawing.Size(100, 20)
        Me.TbName.TabIndex = 0
        '
        'Tv1
        '
        Me.Tv1.Location = New System.Drawing.Point(2, 0)
        Me.Tv1.Name = "Tv1"
        Me.Tv1.Size = New System.Drawing.Size(600, 501)
        Me.Tv1.TabIndex = 1
        '
        'Bt_Save
        '
        Me.Bt_Save.Location = New System.Drawing.Point(527, 513)
        Me.Bt_Save.Name = "Bt_Save"
        Me.Bt_Save.Size = New System.Drawing.Size(75, 23)
        Me.Bt_Save.TabIndex = 6
        Me.Bt_Save.Text = "Save"
        Me.Bt_Save.UseVisualStyleBackColor = True
        '
        'BtLoad
        '
        Me.BtLoad.Location = New System.Drawing.Point(446, 513)
        Me.BtLoad.Name = "BtLoad"
        Me.BtLoad.Size = New System.Drawing.Size(75, 23)
        Me.BtLoad.TabIndex = 7
        Me.BtLoad.Text = "Load"
        Me.BtLoad.UseVisualStyleBackColor = True
        '
        'DS_BT
        '
        Me.DS_BT.DataSetName = "DS_BT"
        Me.DS_BT.Tables.AddRange(New System.Data.DataTable() {Me.DataTable1})
        '
        'DataTable1
        '
        Me.DataTable1.Columns.AddRange(New System.Data.DataColumn() {Me.DataColumn1, Me.DataColumn2, Me.DataColumn3, Me.DataColumn4, Me.DataColumn5})
        Me.DataTable1.TableName = "Table1"
        '
        'DataColumn1
        '
        Me.DataColumn1.AllowDBNull = False
        Me.DataColumn1.AutoIncrement = True
        Me.DataColumn1.ColumnName = "LfNr"
        Me.DataColumn1.DataType = GetType(Integer)
        '
        'DataColumn2
        '
        Me.DataColumn2.ColumnName = "Parrent"
        '
        'DataColumn3
        '
        Me.DataColumn3.ColumnName = "Name"
        '
        'DataColumn4
        '
        Me.DataColumn4.ColumnName = "Typ"
        Me.DataColumn4.DataType = GetType(Integer)
        '
        'DataColumn5
        '
        Me.DataColumn5.ColumnName = "Code"
        '
        'DataGridView1
        '
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(626, 0)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(312, 654)
        Me.DataGridView1.TabIndex = 8
        '
        'OFiD1
        '
        Me.OFiD1.FileName = "OpenFileDialog1"
        '
        'BtÄndern
        '
        Me.BtÄndern.Location = New System.Drawing.Point(260, 38)
        Me.BtÄndern.Name = "BtÄndern"
        Me.BtÄndern.Size = New System.Drawing.Size(75, 23)
        Me.BtÄndern.TabIndex = 6
        Me.BtÄndern.Text = "ändern"
        Me.BtÄndern.UseVisualStyleBackColor = True
        '
        'BT_Visu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(937, 709)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.BtLoad)
        Me.Controls.Add(Me.Bt_Save)
        Me.Controls.Add(Me.Tv1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "BT_Visu"
        Me.Text = "BT_Visu"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.DS_BT, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataTable1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents BtAdd As Button
    Friend WithEvents BtDel As Button
    Friend WithEvents CbTyp As ComboBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents TbName As TextBox
    Friend WithEvents Tv1 As TreeView
    Friend WithEvents Bt_Save As Button
    Friend WithEvents BtLoad As Button
    Private WithEvents DS_BT As DataSet
    Friend WithEvents DataTable1 As DataTable
    Friend WithEvents DataColumn1 As DataColumn
    Friend WithEvents DataColumn2 As DataColumn
    Friend WithEvents DataColumn3 As DataColumn
    Friend WithEvents DataColumn4 As DataColumn
    Friend WithEvents DataColumn5 As DataColumn
    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents OFiD1 As OpenFileDialog
    Friend WithEvents SFD1 As SaveFileDialog
    Friend WithEvents BtÄndern As Button
End Class
