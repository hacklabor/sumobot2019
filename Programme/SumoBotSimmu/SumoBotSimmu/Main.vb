﻿Imports System.IO.Ports
Imports SumoBotSimmu.rechnen

Public Class Main


    Public Structure BotStruct
        Dim BotDurchmesser, BotRadius, RadAbstand, SaugerAbstand, SaugerDurchmesser, Radius_Linienerkennung As Integer
        Dim x, y, Rz_Absolut, Rz_Relativ_MP, X_Sauger1, Y_Sauger1, X_Sauger2, Y_Sauger2 As Double
        Dim BotFarbe, SaugerFarbeEin, SaugerFarbeAus, WinkelFarbe As Color
        Dim VAC1_EIN, VAC2_EIN, VAC1_OK, VAC2_OK, VAC1_OK_Merker, VAC2_OK_Merker As Boolean
        Dim Motor1_Speed, Motor2_Speed As Integer
        Dim Lininenerkennung_OK As Byte 'Bit 0 = Linefolger1 Bit1= Linienfolger2 etc.
        Dim Abstandssensoren_Green As Byte 'Bit0 = 0 Grad(N); Bit1 = 45 Grad(NO);.....Bit7 = 315 Grad(N)
        Dim Abstandssensoren_YellowAlert As Byte 'Bit0 = 0 Grad(N); Bit1 = 45 Grad(NO);.....Bit7 = 315 Grad(N)
        Dim Abstandssensoren_RedAlert As Byte 'Bit0 = 0 Grad(N); Bit1 = 45 Grad(NO);.....Bit7 = 315 Grad(N)
    End Structure

    Public Structure LidarStruct
        Dim P() As Point
        Dim Farbe As Color
        Dim winkel As Double
    End Structure

    Public Structure node
        Public type As Integer
        Public current_child_index As Integer
        Public child_count As Integer
        Public children() As node
        Public Delegate Function actionDelegate(ByVal agent As Object) As Integer
        Public action As actionDelegate
    End Structure

    Public X_SpielfeldOffset, Y_SpielfeldOffset As Single
    Public Bot As BotStruct
    Public Lidar(8) As LidarStruct
    Dim ladenBeendet As Boolean = False
    Dim Scalierung As Double
    Dim imags As Image



    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        NudArenaDurchmesser.Value = My.Settings.ArenaDurchmesser
        NudBotDurchmesser.Value = My.Settings.BotDurchmesser
        NudPfahlDurchmesser.Value = My.Settings.PfahlDurchmesser
        NudRadAbstand.Value = My.Settings.BotRadAbstand
        NudSaugerAbstand.Value = My.Settings.BotSaugerAbstand
        NudSpeed.Value = My.Settings.Speed
        ladenBeendet = True
        Dim bmp As New Bitmap(CInt(NudArenaDurchmesser.Value), CInt(NudArenaDurchmesser.Value))
        imags = bmp
        For i = 0 To 7
            Lidar(i).winkel = 45 * i
        Next






        Lidar(0).Farbe = Color.Yellow
        Lidar(1).Farbe = Color.Violet
        Lidar(2).Farbe = Color.YellowGreen
        Lidar(3).Farbe = Color.Orange
        Lidar(4).Farbe = Color.IndianRed
        Lidar(5).Farbe = Color.Blue
        Lidar(6).Farbe = Color.Green
        Lidar(7).Farbe = Color.DarkOrange



        Reset()

        'Timer1.Start()
    End Sub
    Private Sub Calculate()

        If Bot.VAC1_OK = True And Bot.VAC2_OK = False Then
            rechnen.BotPosAbsolut_REF_Sauger1(Bot)
        End If
        If Bot.VAC2_OK = True And Bot.VAC1_OK = False Then
            rechnen.BotPosAbsolut_REF_Sauger2(Bot)
        End If
        rechnen.SaugerPosAbsolut(Bot)


        Lidar(0).P = rechnen.Abstand_Lidar_Außenbregenzung(Bot.x, Bot.y, CDbl(90 - Bot.Rz_Absolut + 0), NudBotDurchmesser.Value / 2, NudArenaDurchmesser.Value / 2)


        Bild_Zeichnen()
    End Sub

    Private Sub INIT_BOT()
        Bot.x = 0
        Bot.y = (NudArenaDurchmesser.Value / 2 - (NudBotDurchmesser.Value / 2)) * -1
        Bot.Rz_Relativ_MP = 0
        Bot.Rz_Absolut = 0
        Bot.SaugerDurchmesser = 40
        Bot.BotDurchmesser = NudBotDurchmesser.Value
        Bot.RadAbstand = NudRadAbstand.Value
        Bot.SaugerAbstand = NudSaugerAbstand.Value
        Bot.BotFarbe = Color.Gray
        Bot.SaugerFarbeEin = Color.Lime
        Bot.SaugerFarbeAus = Color.LightGreen
        Bot.WinkelFarbe = Color.Black
    End Sub

    Private Sub Reset()
        X_SpielfeldOffset = (NudArenaDurchmesser.Value / 2)
        Y_SpielfeldOffset = (NudArenaDurchmesser.Value / 2)
        INIT_BOT()
        rechnen.SaugerPosAbsolut(Bot)
        Nud_WinkelAbsolut.Value = Bot.Rz_Absolut
        NudBotX.Value = Bot.x
        NudBotY.Value = Bot.y

        Calculate()
    End Sub





    Private Sub einstellungen_save()
        My.Settings.ArenaDurchmesser = NudArenaDurchmesser.Value
        My.Settings.BotDurchmesser = NudBotDurchmesser.Value
        My.Settings.PfahlDurchmesser = NudPfahlDurchmesser.Value
        My.Settings.BotRadAbstand = NudRadAbstand.Value
        My.Settings.BotSaugerAbstand = NudSaugerAbstand.Value
        My.Settings.Speed = NudSpeed.Value

        Dim bmp As New Bitmap(CInt(NudArenaDurchmesser.Value), CInt(NudArenaDurchmesser.Value))
        imags = bmp
        Reset()

    End Sub



    Private Sub Bild_Zeichnen()
        If IsNothing(imags) = True Then Return
        Dim g As Graphics = Graphics.FromImage(imags)
        Dim bru As New SolidBrush(Color.White)
        Dim rect As New RectangleF(0, 0, imags.Width, imags.Height)
        Dim pe As New Pen(bru)
        pe.Color = Color.Black
        pe.Width = 5
        g.Clear(Color.White)

        g.DrawArc(pe, 0, 0, CSng(NudArenaDurchmesser.Value), CSng(NudArenaDurchmesser.Value), 0, 360)


        Zeichnen.drawBot(g, Bot, Lidar, X_SpielfeldOffset, Y_SpielfeldOffset)
        Zeichnen.drawKreis(g, NudPfahlDurchmesser.Value, 0, 0, X_SpielfeldOffset, Y_SpielfeldOffset, Color.Purple)






        PB1.Image = imags
    End Sub





    Private Sub NudBotDurchmesser_ValueChanged(sender As Object, e As EventArgs) Handles NudBotDurchmesser.ValueChanged
        If ladenBeendet = False Then Return
        einstellungen_save()
    End Sub

    Private Sub NudRadAbstand_ValueChanged(sender As Object, e As EventArgs) Handles NudRadAbstand.ValueChanged
        If ladenBeendet = False Then Return
        einstellungen_save()
    End Sub

    Private Sub NudSaugerAbstand_ValueChanged(sender As Object, e As EventArgs) Handles NudSaugerAbstand.ValueChanged
        If ladenBeendet = False Then Return
        einstellungen_save()
    End Sub

    Private Sub NudSpeed_ValueChanged(sender As Object, e As EventArgs) Handles NudSpeed.ValueChanged
        If ladenBeendet = False Then Return
        einstellungen_save()
    End Sub

    Private Sub NudArenaDurchmesser_ValueChanged(sender As Object, e As EventArgs) Handles NudArenaDurchmesser.ValueChanged
        If ladenBeendet = False Then Return
        einstellungen_save()
    End Sub

    Private Sub Nud_WinkelAbsolut_ValueChanged(sender As Object, e As EventArgs) Handles Nud_WinkelAbsolut.ValueChanged
        If Nud_WinkelAbsolut.Value >= 360 Then Nud_WinkelAbsolut.Value = 0
        If Nud_WinkelAbsolut.Value < 0 Then Nud_WinkelAbsolut.Value += 360
        BotDaten_Refresh()

    End Sub

    Private Sub NudPfahlDurchmesser_ValueChanged(sender As Object, e As EventArgs) Handles NudPfahlDurchmesser.ValueChanged
        If ladenBeendet = False Then Return
        einstellungen_save()
    End Sub

    Private Sub NudBotX_ValueChanged(sender As Object, e As EventArgs) Handles NudBotX.ValueChanged
        BotDaten_Refresh()
    End Sub

    Private Sub NudBotY_ValueChanged(sender As Object, e As EventArgs) Handles NudBotY.ValueChanged
        BotDaten_Refresh()
    End Sub



    Private Sub BotDaten_Refresh()

        'Bot.x = NudBotX.Value
        '  Bot.y = NudBotY.Value
        Bot.Rz_Absolut = Nud_WinkelAbsolut.Value
        Bot.Rz_Relativ_MP = rechnen.RzAbsolut_2_RzRelativ(Nud_WinkelAbsolut.Value, Bot.x, Bot.y)
        NudBotRzRelativ.Value = Bot.Rz_Relativ_MP
        Calculate()
    End Sub



    Private Sub Rb_VAC2_OK_CheckedChanged(sender As Object, e As EventArgs) Handles Rb_VAC2_OK.CheckedChanged
        If sender.checked = True Then
            Bot.VAC2_OK = True
            Bot.VAC1_OK = False
            Bild_Zeichnen()
        End If
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        Try
            TbRxRS232.Text = RS232.ReadLine
        Catch ex As Exception
            Return
        End Try


    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        RS232.Open()
        Timer1.Start()
    End Sub

    Private Sub Rb_VAC1_OK_CheckedChanged(sender As Object, e As EventArgs) Handles Rb_VAC1_OK.CheckedChanged
        If sender.checked = True Then
            Bot.VAC1_OK = True
            Bot.VAC2_OK = False
            Bild_Zeichnen()
        End If
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        BT_Visu.Show()
    End Sub

    Private Sub Tb_COM_Port_TextChanged(sender As Object, e As EventArgs) Handles Tb_COM_Port.TextChanged

    End Sub


End Class
