﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Main
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.NudBotDurchmesser = New System.Windows.Forms.NumericUpDown()
        Me.LblBotDurchmesser = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.LblRadAbstand = New System.Windows.Forms.Label()
        Me.NudRadAbstand = New System.Windows.Forms.NumericUpDown()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.LblSaugerAbstand = New System.Windows.Forms.Label()
        Me.NudSaugerAbstand = New System.Windows.Forms.NumericUpDown()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.LblSpeed = New System.Windows.Forms.Label()
        Me.NudSpeed = New System.Windows.Forms.NumericUpDown()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.LblArenaDurchmesser = New System.Windows.Forms.Label()
        Me.NudArenaDurchmesser = New System.Windows.Forms.NumericUpDown()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.LblPfahlDurchmesser = New System.Windows.Forms.Label()
        Me.NudPfahlDurchmesser = New System.Windows.Forms.NumericUpDown()
        Me.PB1 = New System.Windows.Forms.PictureBox()
        Me.Nud_WinkelAbsolut = New System.Windows.Forms.NumericUpDown()
        Me.NudBotX = New System.Windows.Forms.NumericUpDown()
        Me.NudBotY = New System.Windows.Forms.NumericUpDown()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.NudBotRzRelativ = New System.Windows.Forms.NumericUpDown()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Rb_VAC2_OK = New System.Windows.Forms.RadioButton()
        Me.Rb_VAC1_OK = New System.Windows.Forms.RadioButton()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Tb_COM_Port = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.RS232 = New System.IO.Ports.SerialPort(Me.components)
        Me.Button1 = New System.Windows.Forms.Button()
        Me.TbRxRS232 = New System.Windows.Forms.TextBox()
        Me.Button2 = New System.Windows.Forms.Button()
        CType(Me.NudBotDurchmesser, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NudRadAbstand, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NudSaugerAbstand, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NudSpeed, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NudArenaDurchmesser, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NudPfahlDurchmesser, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PB1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Nud_WinkelAbsolut, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NudBotX, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NudBotY, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NudBotRzRelativ, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'NudBotDurchmesser
        '
        Me.NudBotDurchmesser.Location = New System.Drawing.Point(980, 21)
        Me.NudBotDurchmesser.Maximum = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.NudBotDurchmesser.Name = "NudBotDurchmesser"
        Me.NudBotDurchmesser.Size = New System.Drawing.Size(120, 20)
        Me.NudBotDurchmesser.TabIndex = 2
        '
        'LblBotDurchmesser
        '
        Me.LblBotDurchmesser.AutoSize = True
        Me.LblBotDurchmesser.Location = New System.Drawing.Point(834, 23)
        Me.LblBotDurchmesser.Name = "LblBotDurchmesser"
        Me.LblBotDurchmesser.Size = New System.Drawing.Size(88, 13)
        Me.LblBotDurchmesser.TabIndex = 3
        Me.LblBotDurchmesser.Text = "Bot Durchmesser"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(1106, 26)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(23, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "mm"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(1106, 58)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(23, 13)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "mm"
        '
        'LblRadAbstand
        '
        Me.LblRadAbstand.AutoSize = True
        Me.LblRadAbstand.Location = New System.Drawing.Point(834, 55)
        Me.LblRadAbstand.Name = "LblRadAbstand"
        Me.LblRadAbstand.Size = New System.Drawing.Size(69, 13)
        Me.LblRadAbstand.TabIndex = 6
        Me.LblRadAbstand.Text = "Rad Abstand"
        '
        'NudRadAbstand
        '
        Me.NudRadAbstand.Location = New System.Drawing.Point(980, 53)
        Me.NudRadAbstand.Maximum = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.NudRadAbstand.Name = "NudRadAbstand"
        Me.NudRadAbstand.Size = New System.Drawing.Size(120, 20)
        Me.NudRadAbstand.TabIndex = 5
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(1106, 90)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(23, 13)
        Me.Label4.TabIndex = 10
        Me.Label4.Text = "mm"
        '
        'LblSaugerAbstand
        '
        Me.LblSaugerAbstand.AutoSize = True
        Me.LblSaugerAbstand.Location = New System.Drawing.Point(834, 87)
        Me.LblSaugerAbstand.Name = "LblSaugerAbstand"
        Me.LblSaugerAbstand.Size = New System.Drawing.Size(83, 13)
        Me.LblSaugerAbstand.TabIndex = 9
        Me.LblSaugerAbstand.Text = "Sauger Abstand"
        '
        'NudSaugerAbstand
        '
        Me.NudSaugerAbstand.Location = New System.Drawing.Point(980, 85)
        Me.NudSaugerAbstand.Maximum = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.NudSaugerAbstand.Name = "NudSaugerAbstand"
        Me.NudSaugerAbstand.Size = New System.Drawing.Size(120, 20)
        Me.NudSaugerAbstand.TabIndex = 8
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(1106, 122)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(52, 13)
        Me.Label6.TabIndex = 13
        Me.Label6.Text = "Grad/sec"
        '
        'LblSpeed
        '
        Me.LblSpeed.AutoSize = True
        Me.LblSpeed.Location = New System.Drawing.Point(834, 119)
        Me.LblSpeed.Name = "LblSpeed"
        Me.LblSpeed.Size = New System.Drawing.Size(38, 13)
        Me.LblSpeed.TabIndex = 12
        Me.LblSpeed.Text = "Speed"
        '
        'NudSpeed
        '
        Me.NudSpeed.Location = New System.Drawing.Point(980, 117)
        Me.NudSpeed.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me.NudSpeed.Name = "NudSpeed"
        Me.NudSpeed.Size = New System.Drawing.Size(120, 20)
        Me.NudSpeed.TabIndex = 11
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(1106, 191)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(23, 13)
        Me.Label3.TabIndex = 16
        Me.Label3.Text = "mm"
        '
        'LblArenaDurchmesser
        '
        Me.LblArenaDurchmesser.AutoSize = True
        Me.LblArenaDurchmesser.Location = New System.Drawing.Point(834, 188)
        Me.LblArenaDurchmesser.Name = "LblArenaDurchmesser"
        Me.LblArenaDurchmesser.Size = New System.Drawing.Size(97, 13)
        Me.LblArenaDurchmesser.TabIndex = 15
        Me.LblArenaDurchmesser.Text = "ArenaDurchmesser"
        '
        'NudArenaDurchmesser
        '
        Me.NudArenaDurchmesser.Location = New System.Drawing.Point(980, 186)
        Me.NudArenaDurchmesser.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me.NudArenaDurchmesser.Name = "NudArenaDurchmesser"
        Me.NudArenaDurchmesser.Size = New System.Drawing.Size(120, 20)
        Me.NudArenaDurchmesser.TabIndex = 14
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(1106, 223)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(23, 13)
        Me.Label5.TabIndex = 19
        Me.Label5.Text = "mm"
        '
        'LblPfahlDurchmesser
        '
        Me.LblPfahlDurchmesser.AutoSize = True
        Me.LblPfahlDurchmesser.Location = New System.Drawing.Point(834, 220)
        Me.LblPfahlDurchmesser.Name = "LblPfahlDurchmesser"
        Me.LblPfahlDurchmesser.Size = New System.Drawing.Size(93, 13)
        Me.LblPfahlDurchmesser.TabIndex = 18
        Me.LblPfahlDurchmesser.Text = "PfahlDurchmesser"
        '
        'NudPfahlDurchmesser
        '
        Me.NudPfahlDurchmesser.Location = New System.Drawing.Point(980, 218)
        Me.NudPfahlDurchmesser.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me.NudPfahlDurchmesser.Name = "NudPfahlDurchmesser"
        Me.NudPfahlDurchmesser.Size = New System.Drawing.Size(120, 20)
        Me.NudPfahlDurchmesser.TabIndex = 17
        '
        'PB1
        '
        Me.PB1.Location = New System.Drawing.Point(0, 0)
        Me.PB1.Name = "PB1"
        Me.PB1.Size = New System.Drawing.Size(800, 800)
        Me.PB1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PB1.TabIndex = 20
        Me.PB1.TabStop = False
        '
        'Nud_WinkelAbsolut
        '
        Me.Nud_WinkelAbsolut.DecimalPlaces = 2
        Me.Nud_WinkelAbsolut.Increment = New Decimal(New Integer() {15, 0, 0, 0})
        Me.Nud_WinkelAbsolut.Location = New System.Drawing.Point(217, 28)
        Me.Nud_WinkelAbsolut.Maximum = New Decimal(New Integer() {361, 0, 0, 0})
        Me.Nud_WinkelAbsolut.Minimum = New Decimal(New Integer() {1, 0, 0, -2147483648})
        Me.Nud_WinkelAbsolut.Name = "Nud_WinkelAbsolut"
        Me.Nud_WinkelAbsolut.Size = New System.Drawing.Size(120, 20)
        Me.Nud_WinkelAbsolut.TabIndex = 21
        '
        'NudBotX
        '
        Me.NudBotX.DecimalPlaces = 2
        Me.NudBotX.Location = New System.Drawing.Point(1015, 528)
        Me.NudBotX.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me.NudBotX.Minimum = New Decimal(New Integer() {10000, 0, 0, -2147483648})
        Me.NudBotX.Name = "NudBotX"
        Me.NudBotX.Size = New System.Drawing.Size(120, 20)
        Me.NudBotX.TabIndex = 22
        Me.NudBotX.Visible = False
        '
        'NudBotY
        '
        Me.NudBotY.DecimalPlaces = 2
        Me.NudBotY.Location = New System.Drawing.Point(1015, 560)
        Me.NudBotY.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me.NudBotY.Minimum = New Decimal(New Integer() {10000, 0, 0, -2147483648})
        Me.NudBotY.Name = "NudBotY"
        Me.NudBotY.Size = New System.Drawing.Size(120, 20)
        Me.NudBotY.TabIndex = 23
        Me.NudBotY.Visible = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(124, 30)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(58, 13)
        Me.Label7.TabIndex = 24
        Me.Label7.Text = "Rz Absolut"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(966, 566)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(14, 13)
        Me.Label8.TabIndex = 25
        Me.Label8.Text = "Y"
        Me.Label8.Visible = False
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(966, 530)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(14, 13)
        Me.Label9.TabIndex = 26
        Me.Label9.Text = "X"
        Me.Label9.Visible = False
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(924, 632)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(56, 13)
        Me.Label10.TabIndex = 27
        Me.Label10.Text = "Rz Relativ"
        Me.Label10.Visible = False
        '
        'NudBotRzRelativ
        '
        Me.NudBotRzRelativ.DecimalPlaces = 2
        Me.NudBotRzRelativ.Location = New System.Drawing.Point(1015, 630)
        Me.NudBotRzRelativ.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me.NudBotRzRelativ.Minimum = New Decimal(New Integer() {10000, 0, 0, -2147483648})
        Me.NudBotRzRelativ.Name = "NudBotRzRelativ"
        Me.NudBotRzRelativ.Size = New System.Drawing.Size(120, 20)
        Me.NudBotRzRelativ.TabIndex = 28
        Me.NudBotRzRelativ.Visible = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Rb_VAC2_OK)
        Me.GroupBox1.Controls.Add(Me.Rb_VAC1_OK)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.Nud_WinkelAbsolut)
        Me.GroupBox1.Location = New System.Drawing.Point(842, 279)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(372, 100)
        Me.GroupBox1.TabIndex = 32
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "GroupBox1"
        '
        'Rb_VAC2_OK
        '
        Me.Rb_VAC2_OK.AutoSize = True
        Me.Rb_VAC2_OK.Location = New System.Drawing.Point(17, 57)
        Me.Rb_VAC2_OK.Name = "Rb_VAC2_OK"
        Me.Rb_VAC2_OK.Size = New System.Drawing.Size(73, 17)
        Me.Rb_VAC2_OK.TabIndex = 1
        Me.Rb_VAC2_OK.TabStop = True
        Me.Rb_VAC2_OK.Text = "VAC2_OK"
        Me.Rb_VAC2_OK.UseVisualStyleBackColor = True
        '
        'Rb_VAC1_OK
        '
        Me.Rb_VAC1_OK.AutoSize = True
        Me.Rb_VAC1_OK.Location = New System.Drawing.Point(17, 30)
        Me.Rb_VAC1_OK.Name = "Rb_VAC1_OK"
        Me.Rb_VAC1_OK.Size = New System.Drawing.Size(73, 17)
        Me.Rb_VAC1_OK.TabIndex = 0
        Me.Rb_VAC1_OK.TabStop = True
        Me.Rb_VAC1_OK.Text = "VAC1_OK"
        Me.Rb_VAC1_OK.UseVisualStyleBackColor = True
        '
        'Timer1
        '
        '
        'Tb_COM_Port
        '
        Me.Tb_COM_Port.Location = New System.Drawing.Point(896, 406)
        Me.Tb_COM_Port.Name = "Tb_COM_Port"
        Me.Tb_COM_Port.Size = New System.Drawing.Size(100, 20)
        Me.Tb_COM_Port.TabIndex = 33
        Me.Tb_COM_Port.Text = "COM18"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(825, 409)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(49, 13)
        Me.Label11.TabIndex = 34
        Me.Label11.Text = "Com port"
        '
        'RS232
        '
        Me.RS232.BaudRate = 115200
        Me.RS232.PortName = "COM18"
        Me.RS232.ReadTimeout = 1
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(1002, 405)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 35
        Me.Button1.Text = "Button1"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'TbRxRS232
        '
        Me.TbRxRS232.Location = New System.Drawing.Point(0, 806)
        Me.TbRxRS232.Name = "TbRxRS232"
        Me.TbRxRS232.Size = New System.Drawing.Size(1233, 20)
        Me.TbRxRS232.TabIndex = 36
        Me.TbRxRS232.Text = "COM1"
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(1287, 76)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 37
        Me.Button2.Text = "Button2"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Main
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1782, 1039)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.TbRxRS232)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Tb_COM_Port)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.NudBotRzRelativ)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.NudBotY)
        Me.Controls.Add(Me.NudBotX)
        Me.Controls.Add(Me.PB1)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.LblPfahlDurchmesser)
        Me.Controls.Add(Me.NudPfahlDurchmesser)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.LblArenaDurchmesser)
        Me.Controls.Add(Me.NudArenaDurchmesser)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.LblSpeed)
        Me.Controls.Add(Me.NudSpeed)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.LblSaugerAbstand)
        Me.Controls.Add(Me.NudSaugerAbstand)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.LblRadAbstand)
        Me.Controls.Add(Me.NudRadAbstand)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.LblBotDurchmesser)
        Me.Controls.Add(Me.NudBotDurchmesser)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximumSize = New System.Drawing.Size(1800, 1080)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(1729, 1078)
        Me.Name = "Main"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "BotSimmulation"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.NudBotDurchmesser, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NudRadAbstand, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NudSaugerAbstand, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NudSpeed, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NudArenaDurchmesser, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NudPfahlDurchmesser, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PB1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Nud_WinkelAbsolut, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NudBotX, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NudBotY, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NudBotRzRelativ, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents NudBotDurchmesser As NumericUpDown
    Friend WithEvents LblBotDurchmesser As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents LblRadAbstand As Label
    Friend WithEvents NudRadAbstand As NumericUpDown
    Friend WithEvents Label4 As Label
    Friend WithEvents LblSaugerAbstand As Label
    Friend WithEvents NudSaugerAbstand As NumericUpDown
    Friend WithEvents Label6 As Label
    Friend WithEvents LblSpeed As Label
    Friend WithEvents NudSpeed As NumericUpDown
    Friend WithEvents Label3 As Label
    Friend WithEvents LblArenaDurchmesser As Label
    Friend WithEvents NudArenaDurchmesser As NumericUpDown
    Friend WithEvents Label5 As Label
    Friend WithEvents LblPfahlDurchmesser As Label
    Friend WithEvents NudPfahlDurchmesser As NumericUpDown
    Friend WithEvents PB1 As PictureBox
    Friend WithEvents Nud_WinkelAbsolut As NumericUpDown
    Friend WithEvents NudBotX As NumericUpDown
    Friend WithEvents NudBotY As NumericUpDown
    Friend WithEvents Label7 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents NudBotRzRelativ As NumericUpDown
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Rb_VAC2_OK As RadioButton
    Friend WithEvents Rb_VAC1_OK As RadioButton
    Friend WithEvents Timer1 As Timer
    Friend WithEvents Tb_COM_Port As TextBox
    Friend WithEvents Label11 As Label
    Friend WithEvents RS232 As IO.Ports.SerialPort
    Friend WithEvents Button1 As Button
    Friend WithEvents TbRxRS232 As TextBox
    Friend WithEvents Button2 As Button
End Class
