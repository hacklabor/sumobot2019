﻿Public Class rechnen
    Shared Function RzAbsolut_2_RzRelativ(winkel As Double, x As Double, y As Double)
        Dim RzRelativ As Double = 9999
        Dim Alpha As Double = 0
        If x = 0 Then x = 0.00000001
        If y = 0 Then y = 0.00000001
        Alpha = Winkel_von_X_und_Y(x, y)

        If x > 0 And y > 0 Then
            'Quadrant1

            RzRelativ = 90 + Alpha + winkel

            If RzRelativ <= 180 Then
                Return RzRelativ * -1
            Else
                Return (180 - RzRelativ + 180)

            End If
        End If


        If x > 0 And y < 0 Then
            'Quadrant4

            RzRelativ = Alpha + winkel - 90

            If RzRelativ >= 0 Then
                Return 180 - RzRelativ
            Else
                Return (RzRelativ + 180) * -1

            End If
        End If
        If x < 0 And y > 0 Then
            'Quadrant2

            RzRelativ = Alpha + winkel - 90

            If RzRelativ <= 180 Then
                Return RzRelativ * -1
            Else
                Return (180 - RzRelativ + 180)

            End If
        End If
        If x < 0 And y < 0 Then
            'Quadrant3

            RzRelativ = Alpha + winkel - 90

            If RzRelativ <= 0 Then
                Return RzRelativ * -1

            Else
                Return (RzRelativ) * -1

            End If
        End If
        Return RzRelativ



    End Function

    Shared Sub SaugerPosAbsolut(ByRef bot As Main.BotStruct)
        Dim x, y As Double
        x = rechnen.X_von_RadiusUndWinkel(bot.SaugerAbstand / 2, bot.Rz_Absolut)
        y = rechnen.Y_von_RadiusUndWinkel(bot.SaugerAbstand / 2, bot.Rz_Absolut)
        bot.X_Sauger1 = bot.x + x
        bot.Y_Sauger1 = bot.y - y
        bot.X_Sauger2 = bot.x - x
        bot.Y_Sauger2 = bot.y + y
    End Sub
    Shared Sub BotPosAbsolut_REF_Sauger1(ByRef bot As Main.BotStruct)
        Dim x, y As Double
        x = rechnen.X_von_RadiusUndWinkel(bot.SaugerAbstand / 2, bot.Rz_Absolut)
        y = rechnen.Y_von_RadiusUndWinkel(bot.SaugerAbstand / 2, bot.Rz_Absolut)
        bot.x = bot.X_Sauger1 - x
        bot.y = bot.Y_Sauger1 + y

    End Sub

    Shared Sub BotPosAbsolut_REF_Sauger2(ByRef bot As Main.BotStruct)
        Dim x, y As Double
        x = rechnen.X_von_RadiusUndWinkel(bot.SaugerAbstand / 2, bot.Rz_Absolut)
        y = rechnen.Y_von_RadiusUndWinkel(bot.SaugerAbstand / 2, bot.Rz_Absolut)
        bot.x = bot.X_Sauger2 + x
        bot.y = bot.Y_Sauger2 - y

    End Sub
    Shared Function X_von_RadiusUndWinkel(radius As Double, winkel As Double)
        Return Math.Cos(GradInBogenmass(winkel)) * radius
    End Function

    Shared Function Y_von_RadiusUndWinkel(radius As Double, winkel As Double)
        Return Math.Sin(GradInBogenmass(winkel)) * radius
    End Function

    Shared Function Winkel_von_X_und_Y(x As Double, Y As Double)
        Dim tanAlpha As Double = Y / x
        Dim Alpha As Double = Math.Atan(tanAlpha)
        Return BogenmassInGrad(Alpha)
    End Function
    Shared Function BogenmassInGrad(bogenmass As Double)


        Return bogenmass * 180 / Math.PI

    End Function
    Shared Function GradInBogenmass(Winkel As Double)

        Return Math.PI * Winkel / 180

    End Function

    Shared Function Abstand_Lidar_Außenbregenzung(x1 As Double, y1 As Double, alpha As Double, RadiusRobot As Double, RadiusArena As Double)
        Dim m, x2(2), y2(2), n, L(2), a, b, c, D As Double
        Dim Schnitt(2) As Point


        Schnitt(0) = New Point(-9999999.9999, -999999999.999)
        Schnitt(1) = New Point(-9999999.9999, -999999999.999)
        'alpha = 90-alpha
        m = LineareSteigung(alpha)
        n = y1 - m * x1
        a = m ^ 2 + 1
        b = 2 * m * n
        c = n ^ 2 - RadiusArena ^ 2
        'Diskriminante D
        D = (b ^ 2 - 4 * a * c) / (4 * a ^ 2)
        'Prüfen ob lösbar
        If D < 0 Then Return Schnitt

        Schnitt(0).X = (-1 * b) / (2 * a) + Math.Sqrt(D)
        Try
            Schnitt(0).Y = m * Schnitt(0).X + n
        Catch ex As Exception

        End Try

        Try
            Schnitt(1).X = (-1 * b) / (2 * a) - Math.Sqrt(D)

            Schnitt(1).Y = m * Schnitt(1).X + n
        Catch ex As Exception

        End Try





        Return Schnitt

    End Function

    Shared Function LineareSteigung(winkel As Double)

        If winkel = 90 Or winkel = 180 Then
            winkel -= 0.1

        End If
        Return Math.Tan(GradInBogenmass(winkel))
    End Function
End Class
