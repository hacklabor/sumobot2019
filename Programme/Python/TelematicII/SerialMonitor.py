
import serial
from PyQt5.QtCore import QObject, pyqtSignal
import threading

class SerialMonitor(QObject):
    bufferUpdated = pyqtSignal(str)

    def __init__(self):
        super(SerialMonitor, self).__init__()
        self.running = False
        self.thread = threading.Thread(target=self.serial_monitor_thread)

    def start(self):
        self.running = True
        self.thread.start()

    def stop(self):
        self.running = False

    def serial_monitor_thread(self):
        while self.running is True:
            serialPort = "COM18"
            baudRate = 460800
            ser = serial.Serial(serialPort, baudRate)
            msg = ser.readline()

            if msg:
                try:
                    self.bufferUpdated.emit(str(msg))
                except ValueError:
                    print('Wrong data')
            else:
                pass
            ser.close()
