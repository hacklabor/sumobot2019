import sys
import string


from PyQt5.QtCore import pyqtSlot
from PyQt5.QtWidgets import QApplication, QDialog,QTableWidgetItem
from PyQt5.uic import loadUi
from SerialMonitor import*


class SumobotMAIN(QDialog):
    o=0
    def __init__(self):
        super(SumobotMAIN, self).__init__()
        loadUi('Sumobot.ui', self)
        self.setWindowTitle('Sumobot 2019 Telematik')


        self.monitor = SerialMonitor()
        self.monitor.bufferUpdated.connect(self.update)
        self.startButton.clicked.connect(self.monitor.start)
        self.stopButton.clicked.connect(self.monitor.stop)
        #self.clearBufferButton.clicked.connect(self.clear)

    def update(self, msg):
        self.o +=1
        print(str(self.o)+str(msg))

        dat = msg.split(';')

        Data=''
        ha=''
        if dat[1] == '#H#':
            Data = dat

        if dat[1] == '#S#':
            Data = dat

        z=0
        offset=2
        for item in Data:
            if len(Data)>offset:
                try:

                    self.table1Widget.setItem (z,0,QTableWidgetItem(Data[offset]))
                    self.table1Widget.setItem(z, 1, QTableWidgetItem(str(int(Data[offset], base=16))))
                    offset +=1
                except:
                    foo=42

            z+=1






if __name__ == "__main__":
    app = QApplication(sys.argv)
    widget = SumobotMAIN()
    widget.show()
    sys.exit(app.exec_())


